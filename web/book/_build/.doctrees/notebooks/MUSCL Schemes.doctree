����      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�MUSCL Schemes�h]�h	�Text����MUSCL Schemes�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h�line�M'�source��nF:\projects\facultad\posgrado\hidrodinamica-de-cuerpos-de-agua-fich-unl\web\book\notebooks\MUSCL Schemes.ipynb�hhubh	�	paragraph���)��}�(h�TIn this notebook, we will explore high-order finite volume via MUSCL reconstruction.�h]�h�TIn this notebook, we will explore high-order finite volume via MUSCL reconstruction.�����}�(h�TIn this notebook, we will explore high-order finite volume via MUSCL reconstruction.�hh.hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhubh-)��}�(h��To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.�h]�(h�>To run each of the following cells, use the keyboard shortcut �����}�(h�>To run each of the following cells, use the keyboard shortcut �hh=hhh*Nh)Nubh	�strong���)��}�(h�SHIFT�h]�h�SHIFT�����}�(h�SHIFT�hhHhhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hFh)M'h*h+hh=hhubh� + �����}�(h� + �hh=hhh*Nh)NubhG)��}�(h�ENTER�h]�h�ENTER�����}�(h�ENTER�hh\hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hFh)M'h*h+hh=hhubh�, press the button �����}�(h�, press the button �hh=hhh*Nh)Nubh	�literal���)��}�(h�Run�h]�h�Run�����}�(hhhhrhhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M'h*h+hh=hhubh�# in the toolbar or find the option �����}�(h�# in the toolbar or find the option �hh=hhh*Nh)Nubhq)��}�(h�Cell > Run Cells�h]�h�Cell > Run Cells�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M'h*h+hh=hhubh�, from the menu bar. For more shortcuts, see �����}�(h�, from the menu bar. For more shortcuts, see �hh=hhh*Nh)Nubhq)��}�(h�Help > Keyboard Shortcuts�h]�h�Help > Keyboard Shortcuts�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M'h*h+hh=hhubh�.�����}�(h�.�hh=hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhubh-)��}�(h�MTo get started, import the required Python modules by running the cell below.�h]�h�MTo get started, import the required Python modules by running the cell below.�����}�(hh�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhub�myst_nb.nodes��CellNode���)��}�(hhh]�h��CellInputNode���)��}�(hhh]�h	�literal_block���)��}�(h��# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb�h]�h��# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb�����}�(hhhh�ubah}�(h]�h ]�h"]�h$]�h&]��	xml:space��preserve��language��ipython3�uh(h�hh�hhh*h+h)K ubah}�(h]�h ]��
cell_input�ah"]�h$]�h&]�uh(h�h)M"Nh*h+hh�hhubah}�(h]�h ]��cell�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�MWe wil implement finite-volume methods based on MUSCL reconstruction to solve�h]�h�MWe wil implement finite-volume methods based on MUSCL reconstruction to solve�����}�(h�MWe wil implement finite-volume methods based on MUSCL reconstruction to solve�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M2uh*h+hhhhubh-)��}�(h��\begin{align}
	\frac{\partial u}{\partial t} + \frac{\partial u}{\partial x} = 0,
\end{align}
on a grid $x\in[0,2]$ with periodic boundary conditions.�h]�(h�\begin{align}�����}�(h�\begin{align}�hh�hhh*Nh)Nubh�
�����}�(hhhh�hhh*Nh)Nubh�B\frac{\partial u}{\partial t} + \frac{\partial u}{\partial x} = 0,�����}�(h�B\frac{\partial u}{\partial t} + \frac{\partial u}{\partial x} = 0,�hh�hhh*Nh)Nubh�
�����}�(hhhh�hhh*h+h)K ubh�\end{align}�����}�(h�\end{align}�hh�hhh*Nh)Nubh�
�����}�(hhhh�hhh*h+h)K ubh�
on a grid �����}�(h�
on a grid �hh�hhh*Nh)Nubh	�math���)��}�(h�	x\in[0,2]�h]�h�	x\in[0,2]�����}�(hhhj#  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(j!  h)M4uh*h+hh�hhubh�# with periodic boundary conditions.�����}�(h�# with periodic boundary conditions.�hh�hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M4uh*h+hhhhubh-)��}�(h��Following the example in the textbook, we implement ``residual`` to compute the residual using the second-order upwind-biased method. For the advection scheme, we use a Riemann solver of the form $F(u_L,u_R) = F(u_L)$.�h]�(h�4Following the example in the textbook, we implement �����}�(h�4Following the example in the textbook, we implement �hj<  hhh*Nh)Nubhq)��}�(h�residual�h]�h�residual�����}�(hhhjE  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M9uh*h+hj<  hhubh�� to compute the residual using the second-order upwind-biased method. For the advection scheme, we use a Riemann solver of the form �����}�(h�� to compute the residual using the second-order upwind-biased method. For the advection scheme, we use a Riemann solver of the form �hj<  hhh*Nh)Nubj"  )��}�(h�F(u_L,u_R) = F(u_L)�h]�h�F(u_L,u_R) = F(u_L)�����}�(hhhjX  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(j!  h)M9uh*h+hj<  hhubh�.�����}�(hh�hj<  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M9uh*h+hhhhubh-)��}�(h��**Note**: Only ``dl`` is implemented due to the upwind Riemann solver. Later, you will need to implement the right slope ``dr``.�h]�(hh����}�(hhhjp  hhh*Nh)NubhG)��}�(h�Note�h]�h�Note�����}�(h�Note�hjw  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hFh)M;uh*h+hjp  hhubh�: Only �����}�(h�: Only �hjp  hhh*Nh)Nubhq)��}�(h�dl�h]�h�dl�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M;uh*h+hjp  hhubh�d is implemented due to the upwind Riemann solver. Later, you will need to implement the right slope �����}�(h�d is implemented due to the upwind Riemann solver. Later, you will need to implement the right slope �hjp  hhh*Nh)Nubhq)��}�(h�dr�h]�h�dr�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M;uh*h+hjp  hhubh�.�����}�(hh�hjp  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M;uh*h+hhhhubh-)��}�(h��Complete the functions below. For any three values of ``u``, ``u0``, ``u1`` and ``u2`` are ordered from left to right in ``get_slope``. For example, $u_{i-2}, u_{i-1}, u_i$.�h]�(h�6Complete the functions below. For any three values of �����}�(h�6Complete the functions below. For any three values of �hj�  hhh*Nh)Nubhq)��}�(h�u�h]�h�u�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M=uh*h+hj�  hhubh�, �����}�(h�, �hj�  hhh*Nh)Nubhq)��}�(h�u0�h]�h�u0�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M=uh*h+hj�  hhubh�, �����}�(hj�  hj�  hhh*h+h)K ubhq)��}�(h�u1�h]�h�u1�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M=uh*h+hj�  hhubh� and �����}�(h� and �hj�  hhh*Nh)Nubhq)��}�(h�u2�h]�h�u2�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M=uh*h+hj�  hhubh�# are ordered from left to right in �����}�(h�# are ordered from left to right in �hj�  hhh*Nh)Nubhq)��}�(h�	get_slope�h]�h�	get_slope�����}�(hhhj
  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M=uh*h+hj�  hhubh�. For example, �����}�(h�. For example, �hj�  hhh*Nh)Nubj"  )��}�(h�u_{i-2}, u_{i-1}, u_i�h]�h�u_{i-2}, u_{i-1}, u_i�����}�(hhhj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(j!  h)M=uh*h+hj�  hhubh�.�����}�(hh�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M=uh*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hXi  def get_slope(u0, u1, u2, b):
    # Linear reconstruction
    delta = # Complete definition for delta_i
    return delta

def residual(u, a, dx):
    # Initialize residual vector
    res = 0.0*u
    b = 0.0
    for i in range(n):
        # Interface i+1/2
        if i == n - 1:
            # Enforce periodic BC at x=L
            dl = get_slope(u[i - 1], u[i], u[0], b)
        else:
            dl = get_slope(u[i - 1], u[i], u[i + 1], b)
            
        # Compute u_i+1/2,L
        ul = 
        
        # Compute f_i+1/2=a*uL using the upwind Riemann solver
        fR = a*ul
        
        # Interface i-i/2
        dl = get_slope(u[i - 2], u[i - 1], u[i], b) 
        
        # Compute u_i-1/2,L
        ul = 
        
        # Computr f_i-1/2=a*uL using the upwind Rieman solver
        fL = 
        
        res[i] = -(fupw_R - fupw_L)/dx
    return res�h]�hXi  def get_slope(u0, u1, u2, b):
    # Linear reconstruction
    delta = # Complete definition for delta_i
    return delta

def residual(u, a, dx):
    # Initialize residual vector
    res = 0.0*u
    b = 0.0
    for i in range(n):
        # Interface i+1/2
        if i == n - 1:
            # Enforce periodic BC at x=L
            dl = get_slope(u[i - 1], u[i], u[0], b)
        else:
            dl = get_slope(u[i - 1], u[i], u[i + 1], b)
            
        # Compute u_i+1/2,L
        ul = 
        
        # Compute f_i+1/2=a*uL using the upwind Riemann solver
        fR = a*ul
        
        # Interface i-i/2
        dl = get_slope(u[i - 2], u[i - 1], u[i], b) 
        
        # Compute u_i-1/2,L
        ul = 
        
        # Computr f_i-1/2=a*uL using the upwind Rieman solver
        fL = 
        
        res[i] = -(fupw_R - fupw_L)/dx
    return res�����}�(hhhj;  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj8  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)MB�h*h+hj5  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�uTo advance the solution in time, we provide the second-order midpoint method from the Time-Stepping Methods notebook.�h]�h�uTo advance the solution in time, we provide the second-order midpoint method from the Time-Stepping Methods notebook.�����}�(h�uTo advance the solution in time, we provide the second-order midpoint method from the Time-Stepping Methods notebook.�hjW  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)MR�h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h��def advance_solution(ut, a, dx, dt, tf):
    t = 0
    while t < tf:
        r = residual(ut, a, dx)
        um = ut + 0.5*dt*r
        rm = residual(um, a, dx)
        uf = ut + 1.0*dt*rm
        ut = 1.0*uf
        t += dt 
    return ut�h]�h��def advance_solution(ut, a, dx, dt, tf):
    t = 0
    while t < tf:
        r = residual(ut, a, dx)
        um = ut + 0.5*dt*r
        rm = residual(um, a, dx)
        uf = ut + 1.0*dt*rm
        ut = 1.0*uf
        t += dt 
    return ut�����}�(hhhjl  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hji  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)Mb�h*h+hjf  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�RComplete the following cell to set the initial parameters of the problem. Consider�h]�h�RComplete the following cell to set the initial parameters of the problem. Consider�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jr h*h+hhhhubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(hhh]�h-)��}�(h�``a = 1.0``�h]�hq)��}�(h�a = 1.0�h]�h�a = 1.0�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Js h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Js h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Js h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�``n = 300``�h]�hq)��}�(h�n = 300�h]�h�n = 300�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jt h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jt h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Jt h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�``L = 2.0``�h]�hq)��}�(h�L = 2.0�h]�h�L = 2.0�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Ju h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Ju h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Ju h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�``dt = 2e-3``�h]�hq)��}�(h�	dt = 2e-3�h]�h�	dt = 2e-3�����}�(hhhj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jv h*h+hj  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jv h*h+hj   hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Jv h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h��``tf = 2.0``
The initial solution profile is given by
\begin{align}
  u(x, 0) &= 
  \begin{cases}
  	e^{-20(x-0.5)^2} & \text{if } x < 1.2, \\ 
  	1 & \text{if } 1.2 < x < 1.5,	 \\
  	0 & \text{otherwise}.
  \end{cases}
\end{align}�h]�(hq)��}�(h�tf = 2.0�h]�h�tf = 2.0�����}�(hhhj(  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jw h*h+hj$  hhubh�
�����}�(hhhj$  hhh*Nh)Nubh�(The initial solution profile is given by�����}�(h�(The initial solution profile is given by�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh�\begin{align}�����}�(h�\begin{align}�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh�
u(x, 0) &=�����}�(h�
u(x, 0) &=�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh�\begin{cases}�����}�(h�\begin{cases}�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh�(e^{-20(x-0.5)^2} & \text{if } x < 1.2, \�����}�(h�(e^{-20(x-0.5)^2} & \text{if } x < 1.2, \�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh� 1 & \text{if } 1.2 < x < 1.5,	 \�����}�(h� 1 & \text{if } 1.2 < x < 1.5,	 \�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh�0 & \text{otherwise}.�����}�(h�0 & \text{otherwise}.�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh�\end{cases}�����}�(h�\end{cases}�hj$  hhh*Nh)Nubh�
�����}�(hhhj$  hhh*h+h)K ubh�\end{align}�����}�(h�\end{align}�hj$  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jw h*h+hj!  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Jw h*h+hj�  hhubeh}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Js h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hX  a = 
n = 
L = 
dt = 
tf = 

# Calculate mesh spacing and grid points
dx = L/n 
x = np.arange(0, L, dx)

# Initialize the solution
u0 = np.zeros(n)
for i, xval in enumerate(x):
    if xval < 1.2:
        u0[i] = 
    elif 1.2 < xval < 1.5:
        u0[i] = 
    else:
        u0[i] = �h]�hX  a = 
n = 
L = 
dt = 
tf = 

# Calculate mesh spacing and grid points
dx = L/n 
x = np.arange(0, L, dx)

# Initialize the solution
u0 = np.zeros(n)
for i, xval in enumerate(x):
    if xval < 1.2:
        u0[i] = 
    elif 1.2 < xval < 1.5:
        u0[i] = 
    else:
        u0[i] = �����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�8 h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�^Run the following cell to advance the solution using the time-stepping function provided above�h]�h�^Run the following cell to advance the solution using the time-stepping function provided above�����}�(h�^Run the following cell to advance the solution using the time-stepping function provided above�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�_ h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h�+u_adv = advance_solution(u0, a, dx, dt, tf)�h]�h�+u_adv = advance_solution(u0, a, dx, dt, tf)�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�� h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�[Compute the total variation (TV) of the initial and the final solution. Is this method TVD?�h]�h�[Compute the total variation (TV) of the initial and the final solution. Is this method TVD?�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�� h*h+hhhhubh-)��}�(h�2Complete the TV calculation of the final solution.�h]�h�2Complete the TV calculation of the final solution.�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�� h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h��tv_u0 = np.sum(np.abs(u0[1:] - u0[:-1]))
tv_uf = 

if tv_uf <= tv_u0:
    print('Scheme is TVD')
else:
    print('Scheme is not TVD')�h]�h��tv_u0 = np.sum(np.abs(u0[1:] - u0[:-1]))
tv_uf = 

if tv_uf <= tv_u0:
    print('Scheme is TVD')
else:
    print('Scheme is not TVD')�����}�(hhhj  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�� h*h+hj  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�\Plot the solution of the second-order upwind-biased scheme and compare with initial solution�h]�h�\Plot the solution of the second-order upwind-biased scheme and compare with initial solution�����}�(h�\Plot the solution of the second-order upwind-biased scheme and compare with initial solution�hj*  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�� h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h��plt.figure(0)
plt.plot(x, u0, 'k', lw=1, label=r'$u(x,0)$')
plt.plot(x, u_adv, 'o', color='#bd0c00', ms=1.5, label=r'$u(x,t^*)$')
plt.legend()�h]�h��plt.figure(0)
plt.plot(x, u0, 'k', lw=1, label=r'$u(x,0)$')
plt.plot(x, u_adv, 'o', color='#bd0c00', ms=1.5, label=r'$u(x,t^*)$')
plt.legend()�����}�(hhhj?  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj<  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�" h*h+hj9  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h��To preserve the monotonicity of the scheme, we need to define limiter functions. Complete the ``van_leer`` and ``superbee`` limiters using the formulations shown in the textbook.�h]�(h�^To preserve the monotonicity of the scheme, we need to define limiter functions. Complete the �����}�(h�^To preserve the monotonicity of the scheme, we need to define limiter functions. Complete the �hj[  hhh*Nh)Nubhq)��}�(h�van_leer�h]�h�van_leer�����}�(hhhjd  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)J�I h*h+hj[  hhubh� and �����}�(h� and �hj[  hhh*Nh)Nubhq)��}�(h�superbee�h]�h�superbee�����}�(hhhjw  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)J�I h*h+hj[  hhubh�7 limiters using the formulations shown in the textbook.�����}�(h�7 limiters using the formulations shown in the textbook.�hj[  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�I h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h�odef minmod(r):
    return max([0, min([1, r])])
    
def van_leer(r):
    return 

def superbee(r):
    return �h]�h�odef minmod(r):
    return max([0, min([1, r])])
    
def van_leer(r):
    return 

def superbee(r):
    return �����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)Jq h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h��Complete the ``get_slope`` function below to include the limiters following the theory in the textbook, then run the cell. 
**Note:**, you will need to add ``ep`` to any division by ``r`` to avoid infinity.�h]�(h�Complete the �����}�(h�Complete the �hj�  hhh*Nh)Nubhq)��}�(h�	get_slope�h]�h�	get_slope�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)J� h*h+hj�  hhubh�` function below to include the limiters following the theory in the textbook, then run the cell.�����}�(h�` function below to include the limiters following the theory in the textbook, then run the cell.�hj�  hhh*Nh)Nubh�
�����}�(hhhj�  hhh*Nh)Nubhh����}�(hhhj�  hhh*Nh)NubhG)��}�(h�Note:�h]�h�Note:�����}�(h�Note:�hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hFh)J� h*h+hj�  hhubh�, you will need to add �����}�(h�, you will need to add �hj�  hhh*Nh)Nubhq)��}�(h�ep�h]�h�ep�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)J� h*h+hj�  hhubh� to any division by �����}�(h� to any division by �hj�  hhh*Nh)Nubhq)��}�(h�r�h]�h�r�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)J� h*h+hj�  hhubh� to avoid infinity.�����}�(h� to avoid infinity.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J� h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h��def get_slope(u0, u1, u2, b):
    ep = 1e-20
    # Compute ratio of slopes
    r = 
    phi = limiter(r)
    # Compute limited slope
    delta = 
    return delta�h]�h��def get_slope(u0, u1, u2, b):
    ep = 1e-20
    # Compute ratio of slopes
    r = 
    phi = limiter(r)
    # Compute limited slope
    delta = 
    return delta�����}�(hhhj  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J"� h*h+hj  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�hFor each of the limiter functions implemented above, generate a plot of the solution to compare results.�h]�h�hFor each of the limiter functions implemented above, generate a plot of the solution to compare results.�����}�(hj9  hj7  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J2� h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hXA  fig, ax = plt.subplots(figsize=(8, 2.5), ncols=3, sharex=True, sharey=True)

# Minmod limiter
limiter = minmod
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[0].plot(x, u0, 'k', lw=1)
ax[0].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[0].set_ylabel('$u$')
ax[0].set_xlabel('$x$')
ax[0].set_title('minmod')

# van Leer limiter
limiter = van_leer
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[1].plot(x, u0, 'k', lw=1)
ax[1].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[1].set_ylabel('$u$')
ax[1].set_xlabel('$x$')
ax[1].set_title('van Leer')

# Superbee limiter
limiter = superbee
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[2].plot(x, u0, 'k', lw=1)
ax[2].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[2].set_ylabel('$u$')
ax[2].set_xlabel('$x$')
ax[2].set_title('superbee')

plt.tight_layout()�h]�hXA  fig, ax = plt.subplots(figsize=(8, 2.5), ncols=3, sharex=True, sharey=True)

# Minmod limiter
limiter = minmod
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[0].plot(x, u0, 'k', lw=1)
ax[0].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[0].set_ylabel('$u$')
ax[0].set_xlabel('$x$')
ax[0].set_title('minmod')

# van Leer limiter
limiter = van_leer
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[1].plot(x, u0, 'k', lw=1)
ax[1].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[1].set_ylabel('$u$')
ax[1].set_xlabel('$x$')
ax[1].set_title('van Leer')

# Superbee limiter
limiter = superbee
u_adv = advance_solution(u0, a, dx, dt, tf) 
ax[2].plot(x, u0, 'k', lw=1)
ax[2].plot(x, u_adv, 'o', markersize=1.5, color='#bd0c00')
ax[2].set_ylabel('$u$')
ax[2].set_xlabel('$x$')
ax[2].set_title('superbee')

plt.tight_layout()�����}�(hhhjK  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hjH  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)JB h*h+hjE  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubeh}�(h]��muscl-schemes�ah ]�h"]��muscl schemes�ah$]�h&]�uh(h
h)M'h*h+hhhhubh)��}�(hhh]�(h)��}�(h�
Activities�h]�h�
Activities�����}�(hjt  hjr  ubah}�(h]�h ]�h"]�h$]�h&]�uh(hh)JR4 h*h+hjo  ubj�  )��}�(hhh]�(j�  )��}�(hhh]�h-)��}�(h�PWhat are the advantages and disadvantages of increasing the order of the scheme?�h]�h�PWhat are the advantages and disadvantages of increasing the order of the scheme?�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)JS4 h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JS4 h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�<Check whether the schemes with limiter functions are now TVD�h]�h�<Check whether the schemes with limiter functions are now TVD�����}�(hj�  hj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)JT4 h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JT4 h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(hX  Modify the definition of ``residual`` to use a Riemann solver of the form $F(u_L, u_R)$. To achieve this, we have implemented a function named ``riemann_solver``. Complete the code to ompute the ``dr`` slopes at each interface. Templates are provided below.�h]�(h�Modify the definition of �����}�(h�Modify the definition of �hj�  hhh*Nh)Nubhq)��}�(h�residual�h]�h�residual�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)JU4 h*h+hj�  hhubh�% to use a Riemann solver of the form �����}�(h�% to use a Riemann solver of the form �hj�  hhh*Nh)Nubj"  )��}�(h�F(u_L, u_R)�h]�h�F(u_L, u_R)�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(j!  h)JU4 h*h+hj�  hhubh�8. To achieve this, we have implemented a function named �����}�(h�8. To achieve this, we have implemented a function named �hj�  hhh*Nh)Nubhq)��}�(h�riemann_solver�h]�h�riemann_solver�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)JU4 h*h+hj�  hhubh�". Complete the code to ompute the �����}�(h�". Complete the code to ompute the �hj�  hhh*Nh)Nubhq)��}�(h�dr�h]�h�dr�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)JU4 h*h+hj�  hhubh�8 slopes at each interface. Templates are provided below.�����}�(h�8 slopes at each interface. Templates are provided below.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)JU4 h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JU4 h*h+hj�  hhubj�  )��}�(hhh]�(h-)��}�(h�Note:�h]�h�Note:�����}�(h�Note:�hj  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)JV4 h*h+hj  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�h-)��}�(h�sAdditional ``if`` statements need to be added to enforce periodic boundary conditions to compute ``ur`` and ``dr``.�h]�(h�Additional �����}�(h�Additional �hj-  hhh*Nh)Nubhq)��}�(h�if�h]�h�if�����}�(hhhj6  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)JW4 h*h+hj-  hhubh�P statements need to be added to enforce periodic boundary conditions to compute �����}�(h�P statements need to be added to enforce periodic boundary conditions to compute �hj-  hhh*Nh)Nubhq)��}�(h�ur�h]�h�ur�����}�(hhhjI  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)JW4 h*h+hj-  hhubh� and �����}�(h� and �hj-  hhh*Nh)Nubhq)��}�(h�dr�h]�h�dr�����}�(hhhj\  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)JW4 h*h+hj-  hhubh�.�����}�(hh�hj-  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)JW4 h*h+hj*  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JW4 h*h+hj'  hhubj�  )��}�(hhh]�h-)��}�(h�KFor linear advection, the Riemann solver is expected to return only ``ul``.�h]�(h�DFor linear advection, the Riemann solver is expected to return only �����}�(h�DFor linear advection, the Riemann solver is expected to return only �hj}  hhh*Nh)Nubhq)��}�(h�ul�h]�h�ul�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)JX4 h*h+hj}  hhubh�.�����}�(hh�hj}  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)JX4 h*h+hjz  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JX4 h*h+hj'  hhubeh}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JW4 h*h+hj  hhubeh}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JV4 h*h+hj�  hhubeh}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)JS4 h*h+hjo  hhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hX�  def riemann_solver(ul, ur):
    return ul
    
def residual(u, a, dx):
    res = 0.0*u
    b = 0.0
    for i in range(n):
        # Interface i+1/2
        if i == n - 1:
            # Enforce periodic BC at x=L
            dl = get_slope(u[i - 1], u[i], u[0], b)
            dr = 
        elif i == n - 2:
            dl = 
            dr = 
        else:
            dl = get_slope(u[i - 1], u[i], u[i + 1], b)
            dr = 
            
        # Compute u_i+1/2,L and u_i+1/2,R
        ul = u[i] + 0.5*dl
        ur = 
        
        # Compute f_i+1/2 using the upwind Riemann solver
        fR = riemann_solver(ul, ur)
        
        # Interface i-i/2
        dl = get_slope(u[i - 2], u[i - 1], u[i], b) 
        dr = 
        
        # Compute u_i-1/2,L and u_i-1/2,R
        ul = u[i - 1] + 0.5*dl
        ur = 
        
        # Computr f_i-1/2 using the upwind Rieman solver
        fL = riemann_solver(ul, ur)
        
        res[i] = -(fR - fL)/dx
    return res�h]�hX�  def riemann_solver(ul, ur):
    return ul
    
def residual(u, a, dx):
    res = 0.0*u
    b = 0.0
    for i in range(n):
        # Interface i+1/2
        if i == n - 1:
            # Enforce periodic BC at x=L
            dl = get_slope(u[i - 1], u[i], u[0], b)
            dr = 
        elif i == n - 2:
            dl = 
            dr = 
        else:
            dl = get_slope(u[i - 1], u[i], u[i + 1], b)
            dr = 
            
        # Compute u_i+1/2,L and u_i+1/2,R
        ul = u[i] + 0.5*dl
        ur = 
        
        # Compute f_i+1/2 using the upwind Riemann solver
        fR = riemann_solver(ul, ur)
        
        # Interface i-i/2
        dl = get_slope(u[i - 2], u[i - 1], u[i], b) 
        dr = 
        
        # Compute u_i-1/2,L and u_i-1/2,R
        ul = u[i - 1] + 0.5*dl
        ur = 
        
        # Computr f_i-1/2 using the upwind Rieman solver
        fL = riemann_solver(ul, ur)
        
        res[i] = -(fR - fL)/dx
    return res�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)Jb[ h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hjo  hhh*h+h)K ubh-)��}�(h��Rewrite ``riemann_solver`` so that it uses a Roe solver for the Burgers equation. Then, run the following cell to see your implementation�h]�(h�Rewrite �����}�(h�Rewrite �hj�  hhh*Nh)Nubhq)��}�(h�riemann_solver�h]�h�riemann_solver�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jr� h*h+hj�  hhubh�o so that it uses a Roe solver for the Burgers equation. Then, run the following cell to see your implementation�����}�(h�o so that it uses a Roe solver for the Burgers equation. Then, run the following cell to see your implementation�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jr� h*h+hjo  hhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hX  n = 100
L = 1.0
dt = 0.001
tf = 0.5

# Calculate mesh spacing and grid points
dx = L/n 
x = np.arange(0, L, dx)

# Define limiter to use
limiter = minmod

u0 = np.exp(-40 * (x - 1/2)**2)
u_burgers = advance_solution(u0, 1, dx, dt, tf) 

plt.figure()
plt.plot(x, u_burgers)�h]�hX  n = 100
L = 1.0
dt = 0.001
tf = 0.5

# Calculate mesh spacing and grid points
dx = L/n 
x = np.arange(0, L, dx)

# Define limiter to use
limiter = minmod

u0 = np.exp(-40 * (x - 1/2)**2)
u_burgers = advance_solution(u0, 1, dx, dt, tf) 

plt.figure()
plt.plot(x, u_burgers)�����}�(hhhj   ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�� h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hjo  hhh*h+h)K ubeh}�(h]��
activities�ah ]�h"]��
activities�ah$]�h&]�uh(h
h)JR4 h*h+hhhhubeh}�(h]�h ]�h"]�h$]�h&]��source�h+uh(h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jG  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(jl  ji  j!  j  u�	nametypes�}�(jl  Nj!  Nuh}�(ji  hj  jo  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhh�fm_substitutions�}�ub.