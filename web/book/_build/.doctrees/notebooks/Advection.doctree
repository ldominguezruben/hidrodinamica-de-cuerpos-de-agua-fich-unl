���;      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Linear Advection�h]�h	�Text����Linear Advection�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h�line�M'�source��jF:\projects\facultad\posgrado\hidrodinamica-de-cuerpos-de-agua-fich-unl\web\book\notebooks\Advection.ipynb�hhubh	�	paragraph���)��}�(h�[In this notebook, we will explore the first-order upwind scheme for the advection equation.�h]�h�[In this notebook, we will explore the first-order upwind scheme for the advection equation.�����}�(h�[In this notebook, we will explore the first-order upwind scheme for the advection equation.�hh.hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhubh-)��}�(h��To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.�h]�(h�>To run each of the following cells, use the keyboard shortcut �����}�(h�>To run each of the following cells, use the keyboard shortcut �hh=hhh*Nh)Nubh	�strong���)��}�(h�SHIFT�h]�h�SHIFT�����}�(h�SHIFT�hhHhhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hFh)M'h*h+hh=hhubh� + �����}�(h� + �hh=hhh*Nh)NubhG)��}�(h�ENTER�h]�h�ENTER�����}�(h�ENTER�hh\hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hFh)M'h*h+hh=hhubh�, press the button �����}�(h�, press the button �hh=hhh*Nh)Nubh	�literal���)��}�(h�Run�h]�h�Run�����}�(hhhhrhhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M'h*h+hh=hhubh�# in the toolbar or find the option �����}�(h�# in the toolbar or find the option �hh=hhh*Nh)Nubhq)��}�(h�Cell > Run Cells�h]�h�Cell > Run Cells�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M'h*h+hh=hhubh�, from the menu bar. For more shortcuts, see �����}�(h�, from the menu bar. For more shortcuts, see �hh=hhh*Nh)Nubhq)��}�(h�Help > Keyboard Shortcuts�h]�h�Help > Keyboard Shortcuts�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M'h*h+hh=hhubh�.�����}�(h�.�hh=hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhubh-)��}�(h�MTo get started, import the required Python modules by running the cell below.�h]�h�MTo get started, import the required Python modules by running the cell below.�����}�(hh�hh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M'h*h+hhhhub�myst_nb.nodes��CellNode���)��}�(hhh]�h��CellInputNode���)��}�(hhh]�h	�literal_block���)��}�(h��# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb�h]�h��# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb�����}�(hhhh�ubah}�(h]�h ]�h"]�h$]�h&]��	xml:space��preserve��language��ipython3�uh(h�hh�hhh*h+h)K ubah}�(h]�h ]��
cell_input�ah"]�h$]�h&]�uh(h�h)M"Nh*h+hh�hhubah}�(h]�h ]��cell�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�jRun the cell containing the function ``advection_upwind``. Read the comments describing each of the steps.�h]�(h�%Run the cell containing the function �����}�(h�%Run the cell containing the function �hh�hhh*Nh)Nubhq)��}�(h�advection_upwind�h]�h�advection_upwind�����}�(hhhh�hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)M2uh*h+hh�hhubh�1. Read the comments describing each of the steps.�����}�(h�1. Read the comments describing each of the steps.�hh�hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)M2uh*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(hXX  def advection_upwind(a, L, n, dt, tf):
    # Build grid
    dx = L/n
    x = np.linspace(0, L - dx, n)

    # Initialize solution
    u = np.exp(-40*(x-1/2)**2)
    ut = np.zeros(u.shape)

    # Advance solution in time
    t = 0
    while(t < tf):
        for i in range(n):
            # Enforce periodic boundary condition at x=0
            if i == 0:
                ut[i] = u[i] - a*dt/dx*(u[i] - u[n - 1])
            else:
                ut[i] = u[i] - a*dt/dx*(u[i] - u[i - 1])

        u[:] = ut[:]
        t += dt

    plt.plot(x, u, 'o-', markersize=2, label=f'$n={n}$')
    plt.legend()�h]�hXX  def advection_upwind(a, L, n, dt, tf):
    # Build grid
    dx = L/n
    x = np.linspace(0, L - dx, n)

    # Initialize solution
    u = np.exp(-40*(x-1/2)**2)
    ut = np.zeros(u.shape)

    # Advance solution in time
    t = 0
    while(t < tf):
        for i in range(n):
            # Enforce periodic boundary condition at x=0
            if i == 0:
                ut[i] = u[i] - a*dt/dx*(u[i] - u[n - 1])
            else:
                ut[i] = u[i] - a*dt/dx*(u[i] - u[i - 1])

        u[:] = ut[:]
        t += dt

    plt.plot(x, u, 'o-', markersize=2, label=f'$n={n}$')
    plt.legend()�����}�(hhhj  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)MB�h*h+hj  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�YCreate a matplotlib figure and add labels to each axis. Plots will appear in this figure.�h]�h�YCreate a matplotlib figure and add labels to each axis. Plots will appear in this figure.�����}�(hj4  hj2  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)MR�h*h+hhhhubh-)��}�(h�A**Note**: You can always run this cell again to clear the figure.�h]�(hh����}�(hhhj@  hhh*Nh)NubhG)��}�(h�Note�h]�h�Note�����}�(h�Note�hjG  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hFh)MT�h*h+hj@  hhubh�9: You can always run this cell again to clear the figure.�����}�(h�9: You can always run this cell again to clear the figure.�hj@  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)MT�h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h�6plt.figure(0)
plt.xlabel('$x$')
plt.ylabel('$u(x,t)$')�h]�h�6plt.figure(0)
plt.xlabel('$x$')
plt.ylabel('$u(x,t)$')�����}�(hhhjg  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hjd  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)Mb�h*h+hja  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�1Now run the function ``advection_upwind`` setting�h]�(h�Now run the function �����}�(h�Now run the function �hj�  hhh*Nh)Nubhq)��}�(h�advection_upwind�h]�h�advection_upwind�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jr h*h+hj�  hhubh� setting�����}�(h� setting�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jr h*h+hhhhubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(hhh]�h-)��}�(h�*``a``: The advection speed equal to ``1``,�h]�(hq)��}�(h�a�h]�h�a�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Js h*h+hj�  hhubh�: The advection speed equal to �����}�(h�: The advection speed equal to �hj�  hhh*Nh)Nubhq)��}�(h�1�h]�h�1�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Js h*h+hj�  hhubh�,�����}�(h�,�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Js h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Js h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�(``L``: The domain length equal to ``1``,�h]�(hq)��}�(h�L�h]�h�L�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jt h*h+hj�  hhubh�: The domain length equal to �����}�(h�: The domain length equal to �hj�  hhh*Nh)Nubhq)��}�(hj�  h]�h�1�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jt h*h+hj�  hhubh�,�����}�(hj�  hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jt h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Jt h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�1``n``: The number of grid points equal to ``10``,�h]�(hq)��}�(h�n�h]�h�n�����}�(hhhj#  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Ju h*h+hj  hhubh�%: The number of grid points equal to �����}�(h�%: The number of grid points equal to �hj  hhh*Nh)Nubhq)��}�(h�10�h]�h�10�����}�(hhhj6  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Ju h*h+hj  hhubh�,�����}�(hj�  hj  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Ju h*h+hj  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Ju h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�.``dt``: The time step size equal to ``0.005``,�h]�(hq)��}�(h�dt�h]�h�dt�����}�(hhhj[  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jv h*h+hjW  hhubh�: The time step size equal to �����}�(h�: The time step size equal to �hjW  hhh*Nh)Nubhq)��}�(h�0.005�h]�h�0.005�����}�(hhhjn  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jv h*h+hjW  hhubh�,�����}�(hj�  hjW  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jv h*h+hjT  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Jv h*h+hj�  hhubj�  )��}�(hhh]�h-)��}�(h�&``tf``: The final time equal to ``1``.�h]�(hq)��}�(h�tf�h]�h�tf�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jw h*h+hj�  hhubh�: The final time equal to �����}�(h�: The final time equal to �hj�  hhh*Nh)Nubhq)��}�(hj�  h]�h�1�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)Jw h*h+hj�  hhubh�.�����}�(hh�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)Jw h*h+hj�  hhubah}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Jw h*h+hj�  hhubeh}�(h]�h ]�h"]�h$]�h&]�uh(j�  h)Js h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h��# Make sure plot uses figure above
plt.figure(0)

# Assign the corresponding values to the following variables
a =
L = 
n =  
dt = 
tf =

advection_upwind(a, L, n, dt, tf)�h]�h��# Make sure plot uses figure above
plt.figure(0)

# Assign the corresponding values to the following variables
a =
L = 
n =  
dt = 
tf =

advection_upwind(a, L, n, dt, tf)�����}�(hhhj�  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj�  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�8 h*h+hj�  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(hX  Now, call the function ``advection_upwind`` again with 20, 40, 80 and 160 grid points. Keep the rest of the variables the same. What behaviour do you observe? The result must be equal to the figure shown in the Linear Advection section of the Finite Difference chapter.�h]�(h�Now, call the function �����}�(h�Now, call the function �hj�  hhh*Nh)Nubhq)��}�(h�advection_upwind�h]�h�advection_upwind�����}�(hhhj�  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)J�_ h*h+hj�  hhubh�� again with 20, 40, 80 and 160 grid points. Keep the rest of the variables the same. What behaviour do you observe? The result must be equal to the figure shown in the Linear Advection section of the Finite Difference chapter.�����}�(h�� again with 20, 40, 80 and 160 grid points. Keep the rest of the variables the same. What behaviour do you observe? The result must be equal to the figure shown in the Linear Advection section of the Finite Difference chapter.�hj�  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�_ h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h��# Make sure plot uses figure above
plt.figure(0)

# Call advection in this cell as many times as required. 
# You may use a for loop. 
# Note that the figure will be updated above.�h]�h��# Make sure plot uses figure above
plt.figure(0)

# Call advection in this cell as many times as required. 
# You may use a for loop. 
# Note that the figure will be updated above.�����}�(hhhj  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�� h*h+hj  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h�<Create a new figure, and play with different time step sizes�h]�h�<Create a new figure, and play with different time step sizes�����}�(hj1  hj/  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�� h*h+hhhhubh�)��}�(hhh]�h�)��}�(hhh]�h�)��}�(h�6plt.figure(1)
plt.xlabel('$x$')
plt.ylabel('$u(x,t)$')�h]�h�6plt.figure(1)
plt.xlabel('$x$')
plt.ylabel('$u(x,t)$')�����}�(hhhjC  ubah}�(h]�h ]�h"]�h$]�h&]�h�h�h�h�uh(h�hj@  hhh*h+h)K ubah}�(h]�h ]�h�ah"]�h$]�h&]�uh(h�h)J�� h*h+hj=  hhubah}�(h]�h ]�h�ah"]�h$]�h&]��	cell_type��code�uh(h�hhhhh*h+h)K ubh-)��}�(h��Now it's time to write your own code for a central advection scheme. Repeat the exercise described for the first-order advection using ``dt=0.00001``. What can you observe?�h]�(h��Now it’s time to write your own code for a central advection scheme. Repeat the exercise described for the first-order advection using �����}�(h��Now it's time to write your own code for a central advection scheme. Repeat the exercise described for the first-order advection using �hj_  hhh*Nh)Nubhq)��}�(h�
dt=0.00001�h]�h�
dt=0.00001�����}�(hhhjh  hhh*Nh)Nubah}�(h]�h ]�h"]�h$]�h&]�uh(hph)J�� h*h+hj_  hhubh�. What can you observe?�����}�(h�. What can you observe?�hj_  hhh*Nh)Nubeh}�(h]�h ]�h"]�h$]�h&]�uh(h,h)J�� h*h+hhhhubeh}�(h]��linear-advection�ah ]�h"]��linear advection�ah$]�h&]�uh(h
h)M'h*h+hhhhubah}�(h]�h ]�h"]�h$]�h&]��source�h+uh(h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�j�  j�  s�	nametypes�}�j�  Nsh}�j�  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhh�fm_substitutions�}�ub.