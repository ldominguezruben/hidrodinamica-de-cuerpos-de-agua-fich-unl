���a      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�!Pathline, Streakline y Streamline�h]�h	�Text����!Pathline, Streakline y Streamline�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhM'h�f/mnt/d/projects/facultad/posgrado/HCA/web/book/notebooks/HCA04.01-streamline-streakline-pathline.ipynb�hhhhubh	�	paragraph���)��}�(hX  La partícula roja se mueve en un fluido que fluye; su pathline se traza en rojo; la punta del rastro de tinta azul liberada desde el origen sigue a la partícula, pero a diferencia de la línea de pathline estática (que registra el movimiento anterior del punto), la tinta liberada después de que el punto rojo se aleja continúa moviéndose hacia arriba con el flujo. (Esta es una streakline). Las líneas de trazos representan los contornos del campo de velocidad (streamline), mostrando el movimiento de todo el campo al mismo tiempo.�h]�hX  La partícula roja se mueve en un fluido que fluye; su pathline se traza en rojo; la punta del rastro de tinta azul liberada desde el origen sigue a la partícula, pero a diferencia de la línea de pathline estática (que registra el movimiento anterior del punto), la tinta liberada después de que el punto rojo se aleja continúa moviéndose hacia arriba con el flujo. (Esta es una streakline). Las líneas de trazos representan los contornos del campo de velocidad (streamline), mostrando el movimiento de todo el campo al mismo tiempo.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hM"Nhh,hhhhubh.)��}�(h�~![giphy](images/Streaklines_and_pathlines_animation.gif)
https://en.wikipedia.org/wiki/Streamlines,_streaklines,_and_pathlines�h]�(h	�image���)��}�(h�giphy�h]�h}�(h!]�h#]�h%]�h']�h)]��uri��8notebooks/images/Streaklines_and_pathlines_animation.gif��alt�hE�
candidates�}��*�hNsuh+hAhM2uhh,hh=hhubh�
�����}�(hh=hhhNhNubh�Ehttps://en.wikipedia.org/wiki/Streamlines,_streaklines,_and_pathlines�����}�(hh=hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hM2uhh,hhhhubh.)��}�(h��Una $streamline$ es una curva o familia en todas partes tangente al vector de velocidad local en un instante dado. Líneas instantáneas muestran la dirección en la que viajará un elemento fluido sin masa en cualquier momento.�h]�(h�Una �����}�(hhahhhNhNubh	�math���)��}�(h�
streamline�h]�h�
streamline�����}�(hhkhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihMB�hh,hhahhubh�� es una curva o familia en todas partes tangente al vector de velocidad local en un instante dado. Líneas instantáneas muestran la dirección en la que viajará un elemento fluido sin masa en cualquier momento.�����}�(hhahhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hMB�hh,hhhhubh.)��}�(h��Por definición, debemos tener $Vx dr=0$ lo que al expandirse produce la ecuación de streamline para un tiempo determinado $t=t_1$�h]�(h�Por definición, debemos tener �����}�(hh�hhhNhNubhj)��}�(h�Vx dr=0�h]�h�Vx dr=0�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihMR�hh,hh�hhubh�T lo que al expandirse produce la ecuación de streamline para un tiempo determinado �����}�(hh�hhhNhNubhj)��}�(h�t=t_1�h]�h�t=t_1�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihMR�hh,hh�hhubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hMR�hh,hhhhubh	�
math_block���)��}�(h�)\frac{dx}{u}=\frac{dy}{v}=\frac{dz}{w}=ds�h]�h�)\frac{dx}{u}=\frac{dy}{v}=\frac{dz}{w}=ds�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�N�	xml:space��preserve�uh+h�hMb�hh,hhhhubh.)��}�(h�*donde $s$: es el parametro de integración�h]�(h�donde �����}�(hh�hhhNhNubhj)��}�(h�s�h]�h�s�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJr hh,hh�hhubh�!: es el parametro de integración�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJr hh,hhhhubh.)��}�(h��Asi si $(u,v,w)$ son conocidas, debemos integrar con respecto a **s** para $t=t_1$ con la condición inicial $(x_0,y_0,z_0,t_0)$ sobre $s=0$ y de esa manera luego se eleimina $s$�h]�(h�Asi si �����}�(hh�hhhNhNubhj)��}�(h�(u,v,w)�h]�h�(u,v,w)�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�8 hh,hh�hhubh�0 son conocidas, debemos integrar con respecto a �����}�(hh�hhhNhNubh	�strong���)��}�(h�s�h]�h�s�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hJ�8 hh,hh�hhubh� para �����}�(hh�hhhNhNubhj)��}�(h�t=t_1�h]�h�t=t_1�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�8 hh,hh�hhubh� con la condición inicial �����}�(hh�hhhNhNubhj)��}�(h�(x_0,y_0,z_0,t_0)�h]�h�(x_0,y_0,z_0,t_0)�����}�(hj%  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�8 hh,hh�hhubh� sobre �����}�(hh�hhhNhNubhj)��}�(h�s=0�h]�h�s=0�����}�(hj7  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�8 hh,hh�hhubh�# y de esa manera luego se eleimina �����}�(hh�hhhNhNubhj)��}�(hh�h]�h�s�����}�(hjI  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�8 hh,hh�hhubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�8 hh,hhhhubh.)��}�(h��Una $pathline$ es la trayectoria real recorrida por una partícula de fluido individual durante un período de tiempo. Generado como el paso del tiempo; convinent para generar experimentalmente.�h]�(h�Una �����}�(hj\  hhhNhNubhj)��}�(h�pathline�h]�h�pathline�����}�(hjd  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�_ hh,hj\  hhubh�� es la trayectoria real recorrida por una partícula de fluido individual durante un período de tiempo. Generado como el paso del tiempo; convinent para generar experimentalmente.�����}�(hj\  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�_ hh,hhhhubh.)��}�(h�fLa pathline se define mediante la integración de la relación entre la velocidad y el desplazamiento.�h]�h�fLa pathline se define mediante la integración de la relación entre la velocidad y el desplazamiento.�����}�(hj|  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�� hh,hhhhubh�)��}�(h�0\frac{dx}{dt}=u;\frac{dy}{dt}=v; \frac{dz}{dt}=w�h]�h�0\frac{dx}{dt}=u;\frac{dy}{dt}=v; \frac{dz}{dt}=w�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�Nh�h�uh+h�hJ�� hh,hhhhubh.)��}�(h�mIntegrando $u,v,w$ con respecto a $t$ usando la condición inicial $(x_0,y_0,z_0,t_0)$ y luego elimanamos $t$�h]�(h�Integrando �����}�(hj�  hhhNhNubhj)��}�(h�u,v,w�h]�h�u,v,w�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�� hh,hj�  hhubh� con respecto a �����}�(hj�  hhhNhNubhj)��}�(h�t�h]�h�t�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�� hh,hj�  hhubh� usando la condición inicial �����}�(hj�  hhhNhNubhj)��}�(h�(x_0,y_0,z_0,t_0)�h]�h�(x_0,y_0,z_0,t_0)�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�� hh,hj�  hhubh� y luego elimanamos �����}�(hj�  hhhNhNubhj)��}�(hj�  h]�h�t�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�� hh,hj�  hhubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�� hh,hhhhubh.)��}�(h��Un $Streakline$ es el lugar geométrico de las partículas que han pasado anteriormente por un punto prescrito. El tinte inyectado de manera constante en el fluido en un punto fijo se extiende a lo largo de una línea de rayas�h]�(h�Un �����}�(hj�  hhhNhNubhj)��}�(h�
Streakline�h]�h�
Streakline�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�� hh,hj�  hhubh�� es el lugar geométrico de las partículas que han pasado anteriormente por un punto prescrito. El tinte inyectado de manera constante en el fluido en un punto fijo se extiende a lo largo de una línea de rayas�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�� hh,hhhhubh.)��}�(hX6  Para encontrar la línea de trayectoria, utilice el resultado integrado para el tiempo de retención de la línea de trayectoria como parámetro. Ahora, encontramos la constante de integración que hace que la pathline pase por $(x_0,y_0,z_0)$ por una secuencia de tiempos $\tau<t$. Entonces eliminamos $\tau$.�h]�(h��Para encontrar la línea de trayectoria, utilice el resultado integrado para el tiempo de retención de la línea de trayectoria como parámetro. Ahora, encontramos la constante de integración que hace que la pathline pase por �����}�(hj  hhhNhNubhj)��}�(h�(x_0,y_0,z_0)�h]�h�(x_0,y_0,z_0)�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�" hh,hj  hhubh� por una secuencia de tiempos �����}�(hj  hhhNhNubhj)��}�(h�\tau<t�h]�h�\tau<t�����}�(hj%  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�" hh,hj  hhubh�. Entonces eliminamos �����}�(hj  hhhNhNubhj)��}�(h�\tau�h]�h�\tau�����}�(hj7  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�" hh,hj  hhubh�.�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�" hh,hhhhubh.)��}�(h��Las $Timeline$ son las líneas formadas por un conjunto de partículas fluidas que se marcaron en un instante anterior en el tiempo, creando una línea o una curva que se desplaza en el tiempo a medida que las partículas se mueven.�h]�(h�Las �����}�(hjO  hhhNhNubhj)��}�(h�Timeline�h]�h�Timeline�����}�(hjW  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�I hh,hjO  hhubh�� son las líneas formadas por un conjunto de partículas fluidas que se marcaron en un instante anterior en el tiempo, creando una línea o una curva que se desplaza en el tiempo a medida que las partículas se mueven.�����}�(hjO  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�I hh,hhhhubh.)��}�(h�![IPython](images/timeline.PNG)�h]�hB)��}�(h�IPython�h]�h}�(h!]�h#]�h%]�h']�h)]�hM�notebooks/images/timeline.PNG�hOju  hP}�hRj}  suh+hAhJq hh,hjo  hhubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hJq hh,hhhhubh.)��}�(h��Notar que streamline, pathline y streakline son identicas en flujo uniforme
Para flujo no uniforme, el patrón de la streamline cambia con el tiempo, mientras pathlines y streaklines son generadas en cada paso de tiempo�h]�(h�KNotar que streamline, pathline y streakline son identicas en flujo uniforme�����}�(hj�  hhhNhNubh�
�����}�(hj�  hhhNhNubh��Para flujo no uniforme, el patrón de la streamline cambia con el tiempo, mientras pathlines y streaklines son generadas en cada paso de tiempo�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ� hh,hhhhubeh}�(h!]�� pathline-streakline-y-streamline�ah#]�(�tex2jax_ignore��mathjax_ignore�eh%]��!pathline, streakline y streamline�ah']�h)]�uh+h
hM'hh,hhhhubh)��}�(hhh]�(h)��}�(h�	Ejercicio�h]�h�	Ejercicio�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhJ"� hh,hj�  hhubh.)��}�(h�WHallar la streamline, pathline y streakline de la siguiente distribución de velocidad:�h]�h�WHallar la streamline, pathline y streakline de la siguiente distribución de velocidad:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ2� hh,hj�  hhubh�)��}�(h�+ u=\frac{x}{1+t}  ; v=\frac{y}{1+2t}  ; w=0�h]�h�+ u=\frac{x}{1+t}  ; v=\frac{y}{1+2t}  ; w=0�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�Nh�h�uh+h�hJB hh,hj�  hhubh)��}�(hhh]�h)��}�(h�1Dibujamos las funciones encontradas anteriormente�h]�h�1Dibujamos las funciones encontradas anteriormente�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhJR4 hh,hj�  hhubah}�(h!]��1dibujamos-las-funciones-encontradas-anteriormente�ah#]�h%]��1dibujamos las funciones encontradas anteriormente�ah']�h)]�uh+h
hJR4 hh,hj�  hhubh)��}�(hhh]�(h)��}�(h�
Streamline�h]�h�
Streamline�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhJS4 hh,hj�  hhubh�)��}�(h�!y=y_0\left(\frac{x}{x_0}\right)^n�h]�h�!y=y_0\left(\frac{x}{x_0}\right)^n�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�Nh�h�uh+h�hJb[ hh,hj�  hhubh.)��}�(h�con $$n=\frac{1+t}{1+2t}$$�h]�(h�con $�����}�(hj  hhhNhNubhj)��}�(h�n=\frac{1+t}{1+2t}�h]�h�n=\frac{1+t}{1+2t}�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJc[ hh,hj  hhubh�$�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJc[ hh,hj�  hhubeh}�(h!]��
streamline�ah#]�h%]��
streamline�ah']�h)]�uh+h
hJS4 hh,hj�  hhubh)��}�(hhh]�(h)��}�(h�Pathline�h]�h�Pathline�����}�(hj9  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhJr� hh,hj6  hhubh�)��}�(h�7y=y_0\left[1+2\left(\frac{x}{x_0}-1\right)\right]^{1/2}�h]�h�7y=y_0\left[1+2\left(\frac{x}{x_0}-1\right)\right]^{1/2}�����}�hjG  sbah}�(h!]�h#]�h%]�h']�h)]��nowrap���number�Nh�h�uh+h�hJ�� hh,hj6  hhubeh}�(h!]��pathline�ah#]�h%]��pathline�ah']�h)]�uh+h
hJr� hh,hj�  hhubh)��}�(hhh]�(h)��}�(h�
Streakline�h]�h�
Streakline�����}�(hjb  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhJ�� hh,hj_  hhubh.)��}�(h�QPara $t=0$ 
$$\frac{y}{y_0}=\left[1+2\left(\frac{x_0}{x}-1\right)\right]^{-1/2}$$�h]�(h�Para �����}�(hjp  hhhNhNubhj)��}�(h�t=0�h]�h�t=0�����}�(hjx  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�� hh,hjp  hhubh�
�����}�(hjp  hhhNhNubh�$�����}�(hjp  hhhNhNubhj)��}�(h�A\frac{y}{y_0}=\left[1+2\left(\frac{x_0}{x}-1\right)\right]^{-1/2}�h]�h�A\frac{y}{y_0}=\left[1+2\left(\frac{x_0}{x}-1\right)\right]^{-1/2}�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hihJ�� hh,hjp  hhubh�$�����}�(hjp  hhhh,hK ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ�� hh,hj_  hhubh	�	container���)��}�(hhh]�(j�  )��}�(hhh]�h	�literal_block���)��}�(hX$  import numpy as np
import matplotlib.pyplot as plt

## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

n = 1+t/(1+2*t) 
y1 = np.zeros((len(x),1))
m = 0
for i in x:
    y1[m] = y0*(i/x0)**n
    m+=1
    
plt.plot(np.transpose(x), y1, 'or')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')�h]�hX$  import numpy as np
import matplotlib.pyplot as plt

## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

n = 1+t/(1+2*t) 
y1 = np.zeros((len(x),1))
m = 0
for i in x:
    y1[m] = y0*(i/x0)**n
    m+=1
    
plt.plot(np.transpose(x), y1, 'or')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��language��ipython3�h�h�uh+j�  hh,hJ� hj�  hhubah}�(h!]�h#]��
cell_input�ah%]�h']�h)]��
nb_element��cell_code_source�uh+j�  hJ� hh,hj�  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�j�  )��}�(hhh]�j�  )��}�(h�Text(0, 0.5, 'Y')�h]�h�Text(0, 0.5, 'Y')�����}�(hj�  hJ� hh,ubah}�(h!]�h#]�(�output��
text_plain�eh%]�h']�h)]��language��	myst-ansi�h�h�uh+j�  hh,hJ� hj�  ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type��
text/plain�uh+j�  hj�  hJ� hh,ubah}�(h!]�h#]�h%]�h']�h)]��
nb_element��mime_bundle�uh+j�  hJ� hh,hj�  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�j�  )��}�(h�!<Figure size 640x480 with 1 Axes>�h]�h�!<Figure size 640x480 with 1 Axes>�����}�(hj�  hJ� hh,ubah}�(h!]�h#]�(j�  j�  eh%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ� hj�  ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type��
text/plain�uh+j�  hj�  hJ� hh,ubj�  )��}�(hhh]�hB)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]��uri��[_build/jupyter_execute/e19be501adc22f3b6532c89c287f135266f2e3a076e44a3492e73b111ad7c94a.png�hP}�hRj  suh+hAhj  hK hh,ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type��	image/png�uh+j�  hj�  hJ� hh,ubeh}�(h!]�h#]�h%]�h']�h)]��
nb_element�j�  uh+j�  hJ� hh,hj�  hhubeh}�(h!]�h#]��cell_output�ah%]�h']�h)]��
nb_element��cell_code_output�uh+j�  hJ� hh,hj�  hhubeh}�(h!]�h#]��cell�ah%]�h']�h)]��
nb_element��	cell_code��
cell_index�K�
exec_count�K�cell_metadata�}��	slideshow�}��
slide_type��subslide�ssuh+j�  hJ� hh,hj_  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�j�  )��}�(hX,  #PATHline
import numpy as np
import matplotlib.pyplot as plt
del y
## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

y2 = np.zeros((len(x),1))
m = 0
for i in x:
    y2[m] = y0*(1+2*((i/x0)-1))**(1/2)
    m+=1

plt.plot(np.transpose(x), y2, 'ok')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')�h]�hX,  #PATHline
import numpy as np
import matplotlib.pyplot as plt
del y
## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

y2 = np.zeros((len(x),1))
m = 0
for i in x:
    y2[m] = y0*(1+2*((i/x0)-1))**(1/2)
    m+=1

plt.plot(np.transpose(x), y2, 'ok')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')�����}�hjO  sbah}�(h!]�h#]�h%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ�E hjL  hhubah}�(h!]�h#]�j�  ah%]�h']�h)]��
nb_element�j�  uh+j�  hJ�E hh,hjI  hhubj�  )��}�(hhh]�j�  )��}�(hXC  ---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
Cell In[2], line 4
      2 import numpy as np
      3 import matplotlib.pyplot as plt
----> 4 del y
      5 ## Ploteamos
      6 x0 = 1

NameError: name 'y' is not defined�h]�hXC  ---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
Cell In[2], line 4
      2 import numpy as np
      3 import matplotlib.pyplot as plt
----> 4 del y
      5 ## Ploteamos
      6 x0 = 1

NameError: name 'y' is not defined�����}�(hjh  hJ�E hh,ubah}�(h!]�h#]�(j�  �	traceback�eh%]�h']�h)]��language��	ipythontb�h�h�uh+j�  hh,hJ�E hje  hhubah}�(h!]�h#]�j2  ah%]�h']�h)]��
nb_element�j7  uh+j�  hJ�E hh,hjI  hhubeh}�(h!]�h#]�j;  ah%]�h']�h)]��
nb_element�j@  �
cell_index�K�
exec_count�K�cell_metadata�}�jE  }�jG  �subslide�ssuh+j�  hJ�E hh,hj_  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�j�  )��}�(hX,  #STREAKLINE
import numpy as np
import matplotlib.pyplot as plt

## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

y3 = np.zeros((len(x),1))
m = 0
for i in x:
    y3[m] = y0*(1+2*(i/x0-1))**(-0.5)
    m+=1
    
plt.plot(np.transpose(x), y3, 'ob')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')�h]�hX,  #STREAKLINE
import numpy as np
import matplotlib.pyplot as plt

## Ploteamos
x0 = 1
y0 = 1
t  = 0

x = np.linspace(0,10,20)

y3 = np.zeros((len(x),1))
m = 0
for i in x:
    y3[m] = y0*(1+2*(i/x0-1))**(-0.5)
    m+=1
    
plt.plot(np.transpose(x), y3, 'ob')
#plt.show()
plt.xlabel('X')
plt.ylabel('Y')�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ�l hj�  hhubah}�(h!]�h#]�j�  ah%]�h']�h)]��
nb_element�j�  uh+j�  hJ�l hh,hj�  hhubj�  )��}�(hhh]�(j�  )��}�(h��<ipython-input-21-32e7b0dbb8c3>:15: RuntimeWarning: invalid value encountered in double_scalars
  y3[m] = y0*(1+2*(i/x0-1))**(-0.5)
�h]�h��<ipython-input-21-32e7b0dbb8c3>:15: RuntimeWarning: invalid value encountered in double_scalars
  y3[m] = y0*(1+2*(i/x0-1))**(-0.5)
�����}�(hj�  hJ�l hh,ubah}�(h!]�h#]�(j�  �stderr�eh%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ�l hj�  hhubj�  )��}�(hhh]�j�  )��}�(hhh]�j�  )��}�(h�Text(0, 0.5, 'Y')�h]�h�Text(0, 0.5, 'Y')�����}�(hj�  hJ�l hh,ubah}�(h!]�h#]�(j�  j�  eh%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ�l hj�  ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type��
text/plain�uh+j�  hj�  hJ�l hh,ubah}�(h!]�h#]�h%]�h']�h)]��
nb_element�j�  uh+j�  hJ�l hh,hj�  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�hB)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]��uri��[_build/jupyter_execute/05800848b1938bf4ee0417efe8c5dc1a0b3e16bb8e6d4d62f3c0555b4013f6a7.png�hP}�hRj�  suh+hAhj�  hK hh,ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type��	image/png�uh+j�  hj�  hJ�l hh,ubj�  )��}�(hhh]�j�  )��}�(h�!<Figure size 432x288 with 1 Axes>�h]�h�!<Figure size 432x288 with 1 Axes>�����}�(hj�  hJ�l hh,ubah}�(h!]�h#]�(j�  j�  eh%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ�l hj�  ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type�j�  uh+j�  hj�  hJ�l hh,ubeh}�(h!]�h#]�h%]�h']�h)]��
nb_element�j�  uh+j�  hJ�l hh,hj�  hhubeh}�(h!]�h#]�j2  ah%]�h']�h)]��
nb_element�j7  uh+j�  hJ�l hh,hj�  hhubeh}�(h!]�h#]�j;  ah%]�h']�h)]��
nb_element�j@  �
cell_index�K�
exec_count�K�cell_metadata�}�jE  }�jG  �subslide�ssuh+j�  hJ�l hh,hj_  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�j�  )��}�(h��plt.plot(np.transpose(x), y1, 'or', label ='streamline')
plt.plot(np.transpose(x), y2, 'ok', label ='pathline')
plt.plot(np.transpose(x), y3, 'ob', label ='streakline')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend(loc = 2)�h]�h��plt.plot(np.transpose(x), y1, 'or', label ='streamline')
plt.plot(np.transpose(x), y2, 'ok', label ='pathline')
plt.plot(np.transpose(x), y3, 'ob', label ='streakline')
plt.xlabel('X')
plt.ylabel('Y')
plt.legend(loc = 2)�����}�hj4  sbah}�(h!]�h#]�h%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ� hj1  hhubah}�(h!]�h#]�j�  ah%]�h']�h)]��
nb_element�j�  uh+j�  hJ� hh,hj.  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�j�  )��}�(hhh]�j�  )��}�(h�'<matplotlib.legend.Legend at 0x998c1f0>�h]�h�'<matplotlib.legend.Legend at 0x998c1f0>�����}�(hjS  hJ� hh,ubah}�(h!]�h#]�(j�  j�  eh%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ� hjP  ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type�j�  uh+j�  hjM  hJ� hh,ubah}�(h!]�h#]�h%]�h']�h)]��
nb_element�j�  uh+j�  hJ� hh,hjJ  hhubj�  )��}�(hhh]�(j�  )��}�(hhh]�hB)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]��uri��[_build/jupyter_execute/1422680b2f820bd730828db996f5c0956b2bd5d1c3d47fa17bb4739b5a026c51.png�hP}�hRj�  suh+hAhjs  hK hh,ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type�j�  uh+j�  hjp  hJ� hh,ubj�  )��}�(hhh]�j�  )��}�(h�!<Figure size 432x288 with 1 Axes>�h]�h�!<Figure size 432x288 with 1 Axes>�����}�(hj�  hJ� hh,ubah}�(h!]�h#]�(j�  j�  eh%]�h']�h)]��language�j�  h�h�uh+j�  hh,hJ� hj�  ubah}�(h!]�h#]�h%]�h']�h)]��	mime_type�j�  uh+j�  hjp  hJ� hh,ubeh}�(h!]�h#]�h%]�h']�h)]��
nb_element�j�  uh+j�  hJ� hh,hjJ  hhubeh}�(h!]�h#]�j2  ah%]�h']�h)]��
nb_element�j7  uh+j�  hJ� hh,hj.  hhubeh}�(h!]�h#]�j;  ah%]�h']�h)]��
nb_element�j@  �
cell_index�K�
exec_count�K�cell_metadata�}�jE  }�jG  �subslide�ssuh+j�  hJ� hh,hj_  hhubh.)��}�(h�![IPython](images/stream.PNG)�h]�hB)��}�(h�IPython�h]�h}�(h!]�h#]�h%]�h']�h)]�hM�notebooks/images/stream.PNG�hOj�  hP}�hRj�  suh+hAhJ� hh,hj�  hhubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hJ� hh,hj_  hhubeh}�(h!]��
streakline�ah#]�h%]��
streakline�ah']�h)]�uh+h
hJ�� hh,hj�  hhubeh}�(h!]��	ejercicio�ah#]�(j�  j�  eh%]��	ejercicio�ah']�h)]�uh+h
hJ"� hh,hhhhubeh}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�Njs  ��input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�wordcount-words�h	�substitution_definition���)��}�(h�410�h]�h�410�����}�hjC  sbah}�(h!]�h#]�h%]��wordcount-words�ah']�h)]�uh+jA  hh,ub�wordcount-minutes�jB  )��}�(h�2�h]�h�2�����}�hjS  sbah}�(h!]�h#]�h%]��wordcount-minutes�ah']�h)]�uh+jA  hh,ubu�substitution_names�}�(�wordcount-words�j@  �wordcount-minutes�jR  u�refnames�}��refids�}��nameids�}�(j�  j�  j�  j�  j�  j�  j3  j0  j\  jY  j�  j�  u�	nametypes�}�(j�  �j�  �j�  �j3  �j\  �j�  �uh!}�(j�  hj�  j�  j�  j�  j0  j�  jY  j6  j�  j_  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.