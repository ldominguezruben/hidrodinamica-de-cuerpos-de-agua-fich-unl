# Expresiones Lógicas y Operadores

Una **expresión lógica** es una declaración que puede ser verdadera o falsa. Por ejemplo, $ a <b $ es una expresión lógica. Puede ser verdadero o falso según los valores de $ a $ y $ b $. Tenga en cuenta que esto difiere de una ** xpresión matemática** que denota una declaración de verdad. En el ejemplo anterior, la expresión matemática $ a <b $ significa que $ a $ es menor que $ b $, y los valores de $ a $ y $ b $ donde $ a \ ge b $ no están permitidos. Las expresiones lógicas forman la base de la computación, se supone que todas las declaraciones son lógicas en lugar de matemáticas a menos que se indique lo contrario.

En Python, una expresión lógica que sea verdadera calculará el valor "Verdadero". Una expresión falsa calculará el valor "Falso". Este es un nuevo tipo de datos con el que nos encontramos: ** booleano **, que tiene los valores incorporados `Verdadero` y` Falso`. A los efectos de este libro, "Verdadero" es equivalente a 1 y "Falso" es equivalente a 0. Distinguir entre los números 1 y 0 y los valores lógicos "Verdadero" y "Falso" está más allá del alcance de este libro. pero está cubierto en libros más avanzados sobre informática. Las expresiones lógicas se utilizan para plantear preguntas a Python. Por ejemplo, "$ 3 <4 $" es equivalente a, "¿3 es menos que 4?" Dado que esta afirmación es verdadera, Python la computará como 1. Sin embargo, $ 3> 4 $ es falsa, por lo tanto Python la computará como 0.

** Los operadores de comparación ** comparan el valor de dos números y se utilizan para construir expresiones lógicas. Python reserva los símbolos $>,> =, <, <=,! =, == $, para denotar "mayor que", "mayor o igual que", "menor que", "menor o igual que", "no igual "e" igual ", respectivamente. Comencemos con un ejemplo a = 4, b = 2 y veamos la siguiente tabla: 


| Operador | Descripción | Ejemplo | Resultado | 
| :---: | :---: | :---: | :---: |
| > | greather than | a > b | True|
| >= | greater than or equal | a >= b | True|
| < | less than | a < b | False| 
| <= | "less than or equal | a <= b | False|
| != | not equal | a != b | True| 
| == | equal | a == b | False| 


5 == 4

2 < 3

** Los operadores lógicos ** son operaciones entre dos expresiones lógicas que, llamamos $ P $ y $ Q $. Los operadores lógicos fundamentales que usaremos aquí son ** y **, ** o **, y ** no **.

| Operador | Descripción | Ejemplo | Resultado | 
| :---: | :---: | :---: | :---: |
| and | greather than | P and Q | True if both P and Q are True<br>False otherwise|
| or | greater than or equal | P or Q | True if either P or Q is True<br>False otherwise|
| not | less than | not P | True if P is False<br>False if P is True| 
 

Suponiendo que $ P $ es verdadero, use Python para determinar si la expresión $ (P \ AND \ NOT (Q)) \ OR \ (P \ AND \ Q) $ es siempre verdadera independientemente de si o no $ Q $ es cierto. Lógicamente, ¿puedes ver por qué es así? Primero suponga que $ Q $ es cierto:

(1 and not 1) or (1 and 1)

Ahora si asumimos $Q$ falso

(1 and not 0) or (1 and 0)

Al igual que con los operadores aritméticos, los operadores lógicos tienen un orden de operaciones entre sí y en relación con los operadores aritméticos. Todas las operaciones aritméticas se ejecutarán antes que las operaciones de comparación, que se ejecutarán antes que las operaciones lógicas. Los paréntesis se pueden utilizar para cambiar el orden de las operaciones.

1 + 3 > 2 + 5

**WARNING!** En la implementación de la lógica de Python, 1 se usa para denotar verdadero y 0 para falso. Sin embargo, 1 y 0 siguen siendo números. Por lo tanto, Python permitirá abusos como: (3> 2) + (5> 4), que se resolverá en 2. 

(3 > 2) + (5 > 4)

**WARNING!** Aunque en lógica formal, 1 se usa para denotar verdadero y 0 para denotar falso, Python abusa levemente de la notación y tomará cualquier número que no sea igual a 0 para significar verdadero cuando se usa en una operación lógica. Por ejemplo, 3 y 1 se calcularán como verdaderos. No utilice esta función de Python. Utilice siempre 1 para denotar una afirmación verdadera.

