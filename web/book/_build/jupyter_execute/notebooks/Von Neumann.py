# Von Neumann Analysis

In this notebook, we will explore the spectral properties of advection schemes.

To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.

To get started, import the required Python modules by running the cell below.

# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb

We define the amplification factor for the **explicit first-order advection** scheme

def ampf_advection_explicit(sigma, kmdx):
    amp = np.abs(1-sigma+sigma*np.exp(-1j*kmdx))
    return amp

 Then, we define the CFL numbers ``sigmas`` and wavenumbers times grid spacing ``kmdx``

sigma_start = 0.5
sigma_end = 1.0

sigmas = np.linspace(sigma_start, sigma_end, 6)
kmdx = np.linspace(0, np.pi, 100)

Generate the plots by running the following cell

# Initialize plot and colors
fig, ax = plt.subplots()
colors = nb.get_colors(len(sigmas))

for sigma, color in zip(sigmas, colors):
    amp = ampf_advection_explicit(sigma, kmdx)
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.1f}$')

# Display legend
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

# Manually set tick labels as multiples of pi/4 (must change if range changes)
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$'])

Now try the amplification factor for the implicit advection scheme

def ampf_advection_implicit(sigma, kmdx):
    # Complete the following line with the implicit advection amplification factor
    amp = 
    return amp

Since this scheme is unconditionally stable, we show the results for larger values of ``sigma``. Generate curves for six equispaced values of $\sigma\in[0,10]$, following the code in the second cell.

# Generate new values of sigma here
sigma_start =
sigma_end = 

# ...

Complete line ``6`` in the following cell and generate the new plot

# Initialize plot and colors
fig, ax = plt.subplots()
colors = nb.get_colors(len(sigmas))

for sigma, color in zip(sigmas, colors):
    amp = # Complete code here
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.0f}$')

# Display legend
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

# Manually set tick labels as multiples of pi/4 (must change if range changes)
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$'])