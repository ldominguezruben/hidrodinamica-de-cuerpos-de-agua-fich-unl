# Iteraciones
## Bucles usando For

Un **bucle for** es un conjunto de instrucciones que se repite o se itera para cada valor de una secuencia. A veces, los bucles for se denominan **bucles definidos** porque tienen un comienzo y un final predefinidos que están delimitados por la secuencia.

La sintaxis general de un bloque for-loop es la siguiente.

**CONSTRUCTION**: For-loop

```python
for variable de bucle in secuencia:
    bloque de código
```

Un bucle for asigna la **variable de bucle** al primer elemento de la secuencia. Ejecuta todo en el bloque de código. Luego, asigna la variable de bucle al siguiente elemento de la secuencia y vuelve a ejecutar el bloque de código. Continúa hasta que no haya más elementos en la secuencia para asignar.


n = 0
for i in range(1, 4):
    n = n + i

print(n)

**Explicamos**

0. Primero, la función *range (1, 4)* genera una lista de números que comienzan en 1 y terminan en 3. Verifique la descripción de la función *range* y familiarícese con cómo usarla. En una forma muy simple, es * rango (inicio, parada, paso) *, y el *paso* es opcional con 1 como predeterminado.
1. A la variable *n* se le asigna el valor 0.
2. A la variable *i* se le asigna el valor 1.
3. A la variable *n* se le asigna el valor *n + i* ($ 0 + 1 = 1 $).
4. A la variable *i* se le asigna el valor 2.
5. A la variable *n* se le asigna el valor *n + i* ($ 1 + 2 = 3 $).
6. A la variable *i* se le asigna el valor 3.
7. A la variable *n* se le asigna el valor *n + i* ($ 3 + 3 = 6 $).
8. Sin más valores para asignar en la lista, el bucle for termina con
*n = 6*.


Presentamos varios ejemplos más para darle una idea de cómo funcionan los bucles for. Otros ejemplos de secuencias sobre las que podemos iterar incluyen los elementos de una tupla, los caracteres de una cadena y otros tipos de datos secuenciales.

__EXAMPLE:__ Imprimimos la cadena `"banana"`.

for c in "banana":
    print(c)

Otra forma:

s = "banana"
for i in range(len(s)):
    print(s[i])

**EXAMPLE**: Suma de elementos.

s = 0
a = [2, 3, 1, 3, 3]
for i in a:
    s += i 
    
print(s)

La función de Python *sum* ya se ha escrito para manejar el ejemplo anterior. Sin embargo, suponga que desea agregar solo los números pares. ¿Qué cambiaría al bloque de bucle for anterior para manejar esta restricción?

s = 0
for i in range(0, len(a), 2):
    s += a[i]
    
print(s)

**NOTE!** Usamos *paso* como 2 en la función *range* para obtener los índices pares para la lista *a*. Además, un atajo de Python que se usa comúnmente es el operador *+ =*. En Python y muchos otros lenguajes de programación, una declaración como *i + = 1* es equivalente a *i = i + 1* y lo mismo es para otros operadores como *- =*, *\* = *, */ =*.

**Example** Imprimimos la llave de un diccionario. 

dict_a = {"One":1, "Two":2, "Three":3}

for key in dict_a.keys():
    print(key, dict_a[key])

En el ejemplo anterior, primero obtenemos todas las claves usando el método *claves*, y luego usamos la clave para acceder al valor. Alternativamente, podríamos usar el método *item* en un diccionario y obtener la clave y el valor al mismo tiempo que se muestra en el siguiente ejemplo.

for key, value in dict_a.items():
    print(key, value)

Tenga en cuenta que podríamos asignar dos variables de bucle diferentes al mismo tiempo. Hay otros casos en los que podríamos hacer las cosas de manera similar. Por ejemplo, si tenemos dos listas con la misma longitud y queremos recorrerlas, podríamos hacer lo siguiente usando la función *zip*:

a = ["One", "Two", "Three"]
b = [1, 2, 3]

for i, j in zip(a, b):
    print(i, j)

for i in range(5):
    
    if i == 4:
        break
        
    print(i)

The first step in the function *have_digits* assumes that there are no digits in the string *s* (i.e., the output is 0 or False). 

Notice the new keyword *break*. If executed, the *break* keyword immediately stops the most immediate for-loop that contains it; that is, if it is contained in a nested for-loop, then it will only stop the innermost for-loop. In this particular case, the break command is executed if we ever find a digit in the string. The code will still function properly without this statement, but since the task is to find out if there are any digit in *s*, we do not have to keep looking if we find one. Similarly, if a human was given the same task for a long string of characters, that person would not continue looking for digits if he or she already found one. Break statements are used when anything happens in a for-loop that would make you want it to stop early. A less intrusive command is the keyword *continue*, which skips the remaining code in the current iteration of the for-loop, and continues on to the next element of the looping array. See the following example, that we use the keyword *continue* to skip the *print* function to print 2:

for i in range(5):
    
    if i == 2:
        continue
        
    print(i)

## Bucles usando While

Un _while__ o __while indefinido__ es un conjunto de instrucciones que se repite siempre que la expresión lógica asociada sea verdadera. La siguiente es la sintaxis abstracta de un bloque de bucle while.

**CONSTRUCTION:** Bucle While

```python
while <expresion logica>:
    # Code block to be repeated until logical statement is false
    codigo de bloque
```

Cuando Python alcanza un bloque de bucle while, primero determina si la expresión lógica del bucle while es verdadera o falsa. Si la expresión es verdadera, el bloque de código se ejecutará y, una vez ejecutado, el programa regresará a la expresión lógica al comienzo de la instrucción while. Si es falso, el ciclo while terminará.

Determinemos la cantidad de veces que 8 se puede dividir por 2 hasta que el resultado sea menor que 1.

i = 0
n = 8

while n >= 1:
    n /= 2
    i += 1
    
print(f'n = {n}, i = {i}')

**Paso a paso que hicimos**

1. Primero, la variable i se establece en 0.
2. n se establece en 8 y representa el valor actual que estamos dividiendo por 2.
3. Comienza el ciclo while.
4. Python calcula n> = 1 o 8> = 1, lo cual es cierto, por lo que se ejecuta el bloque de código.
5. A n se le asigna n / 2 = 8/2 = 4.
6. i se incrementa a 1.
7. Python calcula n> = 1 o 4> = 1, lo cual es cierto, por lo que se ejecuta el bloque de código.
8. n se asigna n / 2 = 4/2 = 2.
9. i se incrementa a 2.
10. Python calcula n> = 1 o 2> = 1, lo cual es cierto, por lo que se ejecuta el bloque de código.
11. n se asigna n / 2 = 2/2 = 1.
12. i se incrementa a 3.
13. Python calcula n> = 1 o 1> = 1, lo cual es cierto, por lo que se ejecuta el bloque de código.
14. n se asigna n / 2 = 1/2 = 0,5.
15. i se incrementa a 4.
16. Python calcula n> = 1 o 0.5> = 1, lo cual es falso, por lo que el ciclo while termina con i = 4.