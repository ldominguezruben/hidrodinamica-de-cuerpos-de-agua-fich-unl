# TEMA II. Mecánica de fluidos

---

* [**II.1 Ley de Conservación**]
* [**II.2 Ecuación de Navier Stokes**]
* [**II.3 Sistema simplificado**]
* [**II.4 Turbulencia**]
* [**II.4.1 Caos**](HCA02.04.01-caos.ipynb)
* [**II.5 Condiciones de Borde**]

--- 

## Motivación
  

---

