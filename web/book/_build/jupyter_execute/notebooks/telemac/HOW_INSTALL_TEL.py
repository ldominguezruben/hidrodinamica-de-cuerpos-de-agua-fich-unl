#!/usr/bin/env python
# coding: utf-8

# # COMO INSTALAR TELEMAC
# 
# El presente book explica como instalar TELEMAC-MASCARET en la version v8p3r0. 

# In[2]:





# OJO con la identación!!!

# In[3]:


def mi_suma(a, b, c):
    """
    funcion que suma 3 numeros
    Entrada: 3 numeros como a, b, c
    Output: la suma de los 3 numeros a, b, y c
    author: LGDR
    date:
    """
    
# esta es la operación de suma
out = a + b + c
    
return out


# **TIP!!!** Los espacios en blanco de tipo 4 son un nivel de sangría, puede tener una sangría de nivel más profundo cuando tiene una función anidada o una declaración if (veremos esto en el próximo sección). Además, a veces es necesario incorporar sangría o quitarla de un bloque de código. Puede hacer esto seleccionando primero todas las líneas en el bloque de código y presionando *Tab* y *Shift + Tab* para aumentar o disminuir un nivel de sangría.

# Ejemplo de función que no se entiende nada.

# In[4]:


def abc(a, s2, d):
    z = a + s2
    z = z + d
    x = z
    return x


# Las funciones deben ajustarse a un esquema de nomenclatura similar a las variables. Solo pueden contener caracteres alfanuméricos y guiones bajos, y el primer carácter debe ser una letra.
# 
# **TIP!!** Convencionalmente, como los nombres de las variables, los nombres de las funciones deben estar en minúsculas, con palabras separadas por guiones bajos según sea necesario para mejorar la legibilidad.
# 
# **TIP** Es una buena práctica de programación guardar con frecuencia mientras escribe su función. De hecho, muchos programadores informan que ahorran usando el atajo ctrl + s (PC) cada vez que dejan de escribir.
# 
# Probemos la función previa

# In[3]:


d = mi_suma(1, 2, 3)
d


# In[6]:


help(mi_suma)


# **Que hizo Python** Primero recuerde que el operador de asignación trabaja de derecha a izquierda. Esto significa que *mi_suma(1,2,3)* se resuelve antes de la asignación a *d*.
# 
# 1. Python encuentra la función *mi_suma*.
# 2. *mi_suma* toma el primer valor de argumento de entrada 1 y lo asigna a la variable con el nombre *a* (primer nombre de variable en la lista de argumentos de entrada).
# 3. *mi_suma* toma el valor 2 del segundo argumento de entrada y lo asigna a la variable con el nombre *b* (nombre de la segunda variable en la lista de argumentos de entrada).
# 4. *mi_suma* toma el tercer valor de argumento de entrada 3 y lo asigna a la variable con el nombre *c* (tercer nombre de variable en la lista de argumentos de entrada).
# 5. *mi_suma* calcula la suma de *a*, *b* y *c*, que es 1 + 2 + 3 = 6.
# 6. *mi_suma* asigna el valor 6 a la variable *out*.
# 7. *mi_suma* genera el valor contenido en la variable de salida *out*, que es 6.
# 10. *mi_suma (1,2,3)* es equivalente al valor 6, y este valor se asigna a la variable con el nombre *d*.

# Python le da al usuario una enorme libertad para asignar variables a diferentes tipos de datos. Por ejemplo, es posible darle a la variable x un valor de diccionario o un valor flotante. En otros lenguajes de programación, este no es siempre el caso, debe declarar al comienzo de una sesión si x será un diccionario o un tipo flotante, y luego se quedará con él. Por ejemplo, *mi_suma* se construyó asumiendo que los argumentos de entrada eran de tipo numérico, ya sea int o float. Sin embargo, el usuario puede ingresar accidentalmente una lista o cadena en *mi_suma*, lo cual no es correcto. Si intenta ingresar un argumento de entrada de tipo no numérico en *mi_suma*, Python continuará ejecutando la función hasta que algo salga mal.

# Errores comunes.

# In[4]:


d = mi_suma('1', 2, 3)
d


# In[5]:


d = mi_suma(1, 2, [2, 3])
d


# **TIP!!** Recordemos hacer una lectura compresiva de los errores que te da Python. Por lo general, dicen exactamente dónde esta el problema. En este caso, el error dice *---> 11 out = a + b + c*, lo que significa es que hubo un error en mi_suma en la undécima línea. La razón por la que hubo un error es **TypeError**, porque *unsupported operand type(s) for +: 'int' and 'list'*, lo que significa que no pudimos agregar int y list.

# En este punto, no tiene ningún control sobre lo que el usuario asigna a su función como argumentos de entrada y si corresponden a lo que pretendía que fueran esos argumentos de entrada. Entonces, por el momento, escriba sus funciones asumiendo que se usarán correctamente.
# 
# Puede componer funciones asignando llamadas de función como entrada a otras funciones. En el orden de las operaciones, Python ejecutará primero la llamada a la función más interna. También puede asignar expresiones matemáticas como entrada a funciones. En este caso, Python ejecutará primero las expresiones matemáticas.

# Ejemplo de uso de funciones matematicas dentro de su función

# In[6]:


import numpy as np

d = mi_suma(np.sin(np.pi), np.cos(np.pi), np.tan(np.pi))
d


# In[7]:


d = mi_suma(5 + 2, 3 * 4, 12 / 6)
print(d)


# In[19]:


#Comparamos con la notación previa
d = (5 + 2) + 3 * 4 + 12 / 6
d


# Una función que no es necesario argumento:

# In[22]:


def print_hola():
    print('Hola')


# In[23]:


print_hola()


# **TIP** Incluso si no hay un argumento de entrada, cuando llamamos a una función, todavía necesita los paréntesis.
# 
# Para la entrada del argumento, también podemos tener el valor predeterminado. Veamos el siguiente ejemplo.
# 
# **EJEMPLO:** Ejecutamos la siguiente función con y sin una entrada.

# In[27]:


def print_saludos(day = 'Martes', name = 'Lucas'):
    print(f'Saludos, {name}, hoy es {day}')


# In[28]:


print_saludos()


# In[29]:


print_saludos(name = 'Martin', day = 'Miercoles')


# In[31]:


print_saludos(name = 'Juan')

