#!/usr/bin/env python
# coding: utf-8

# # Trabajo Práctico 1
# ## Introducción a Python

# ## Ejercicios
# 
# $\bf{Ejercicio 1}$. Imprima "Me encanta Python" usando Python Shell.
# 
# $\bf{Ejercicio 2}$. Imprima "Me encanta Python" escribiéndolo en un archivo .py y ejecútelo desde la línea de comandos.
# 
# $\bf{Ejercicio 3}$. Escriba *import antigravity* en Ipython Shell, lo llevará a xkcd y verá el gráfico Python.
# 
# $\bf{Ejercicio 4}$. Calcula el área de un triángulo con base 10 y altura 12. Recuerda que el área de un triángulo es la mitad de la base por la altura.
# 
# $\bf{Ejercicio 5}$. Calcule el área de la superficie y el volumen de un cilindro de radio 5 y altura 3.
# 
# $\bf{Ejercicio 6}$. Calcule la distancia entre los puntos $ (3,4) $ y $ (5,9) $. Recuerda que la distancia entre puntos en dos dimensiones es $ \sqrt {(x_2 - x_1) ^ 2 + (y_2 - y_1) ^ 2} $.
# 
# $\bf{Ejercicio 7}$. Escribe una función mi_triangulo(b, h) donde la salida es el área de un triángulo con base, by altura, h. Recuerda que el área de un triángulo es la mitad de la base por la altura. Suponga que b y h son números flotantes de 1 por 1. 
# 
# $\bf{Ejercicio 8}$. Escribe una función my_cylinder(r, h), donde r y h son el radio y la altura de un cilindro, respectivamente, y la salida es una lista [s, v] donde s y v son el área de superficie y el volumen del mismo cilindro , respectivamente. Recuerda que el área de la superficie de un cilindro es $2\pi r2 + 2\pi^rh$ y el volumen es $\pi r2h$. Suponga que r y h son 1 por 1 flotante.
# 
# $\bf{Ejercicio 9}$. Escriba una función mi_n_max(x, n) para devolver una lista que consta de los n elementos más grandes de x. Puede usar la función max de Python. También puede suponer que x es una lista unidimensional sin entradas duplicadas, y que n es un entero estrictamente positivo menor que la longitud de x
# 
# $\bf{Ejercicio 10}$. Grafique las funciones $y1 (x) = 3 + exp − xsin (6x)$ y $y2 (x) = 4 + exp (−x) cos (6x)$ para 0≤x≤5 en un solo eje. Asigne etiquetas, un título y una leyenda al eje de la gráfica.
# 
# $\bf{Ejercicio 11}$. Genere 1000 números aleatorios distribuidos normalmente utilizando la función np.random.randn. Busque la ayuda para la función *plt.hist*. Utilice la función *plt.hist* para trazar un histograma de los números generados aleatoriamente. Utilice la función *plt.hist* para distribuir los números generados aleatoriamente en 10 contenedores. Cree un gráfico de barras de la salida de hist utilizando la función *plt.bar*. Debería verse muy similar a la gráfica producida por *plt.hist*.
# 
# $\bf{Ejercicio 12}$. Suponga que debe generar un conjunto de punto $(x_i,y_i)$ donde $x_1=0$ e $y_1=0$. Los puntos $(x_i,y_i)$ para $i=2,...,n$ se generan de acuerdo con la siguiente relación de probabilidad:
# 
# Con 1% de probabilidad: $x_i=0$; $y_i=0.16y_{i-1}$
# 
# Con 7% de probabilidad: $x_i=0.2x_{i-1}-0.26y_{i-1}$; $y_i=0.23x_{i-1}+0.22y_{i-1}+1.6$
# 
# Con 7% de probabilidad: $x_i=-0.15x_{i-1}+0.28y_{i-1}$; $y_i=0.26x_{i-1}+0.24y_{i-1}+0.44$
# 
# Con 85% de probabilidad: $x_i=0.85x_{i-1}+0.04y_{i-1}$; $y_i=-0.04x_{i-1}+0.85y_{i-1}+1.6$
# 
# Escribe una función my_plantita(n) que genere los puntos $(xi, yi)$ para $i = 1,…, n$ y los grafique usando puntos azules. También use plt.axis ("equal") y plt.axis ("off") para hacer que la grafica se vea mejor.
# 
# 

# $\bf{Ejercicio 13}$. Utilice un esquema de solución de Newton-Raphson para interpolar la profundidad del agua h para una descarga Q dada de un canal trapezoidal. [Revisar](N_R.ipynb)
# Verifica el código con los siguietnes datos reales
# * Colastiné Q=1700m3/s B=400m S=1E-5 n=0.023 m=0.5
# * Paraná Q=18000m3/s B=1500m S=1E-5 n=0.024 m=0.75
# * Zanja Brava Q=1500m3/s B=600m S=1E-5 n=0.02 m=0.75 

# 
# 
# * Escribe una nueva función def interpolate_h (Q, b, m, S, ** kwargs):
# 
# * Defina una estimación inicial de h (por ejemplo, h = 1.0) y un margen de error inicial (por ejemplo, eps = 1.0)
# 
# * Utilice un ciclo while hasta que el margen de error sea insignificante (p. Ej., While eps> 10 ** - 3 :) y calcule:
# 
#     * área húmeda A (vista en clase)
# 
#     * perímetro mojado P (vista en clase)
# 
#     * estimación de descarga de corriente (basada en h): Qk = A ** (5/3) * sqrt (S) / (n_m * P ** (2/3))
# 
#     * error de actualización eps = abs (Q - Qk) / Q
# 
#     * derivada de A: dA_dh = b + 2 * m * h
# 
#     * derivada de P: dP_dh = 2 * m.sqrt (m ** 2 + 1)
# 
#     * función que debería convertirse en cero F = n_m * Q * P ** (2/3) - A ** (5/3) * m.sqrt (S)
# 
#     * su derivada: dF_dh = 2/3 * n_m * Q * P ** (- 1/3) * dP_dh - 5/3 * A ** (2/3) * m.sqrt (S) * dA_dh
# 
#     * actualización de la profundidad del agua h = abs (h - F / dF_dh)
# 
# Implemente una parada de emergencia para evitar iteraciones interminables: ¡el esquema de Newton-Raphson no siempre es estable!
# 
# * Devuelve h y Q (o descarga calculada Qk)

# In[ ]:




