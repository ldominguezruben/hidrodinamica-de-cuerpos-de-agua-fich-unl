#!/usr/bin/env python
# coding: utf-8

# # Trabajo Práctico 4
# ## Lineas de Flujo, descripción de un flujo

# ## Ejercicios
# 
# $\bf{Ejercicio 1}$.Obtenga la pathline, streakline y streamline de una partícula expuesta al flujo plano 2D siguiente:

# $$
# u=\frac{x}{1+t};
# v=y;
# w=0
# $$
# 
# Ademas grafique la pathline encontrada.

# $\bf{Ejercicio 2}$. Aplique el code de Navier-Stokes 2D suponiendo que tiene que evaluar y describir el flujo que se produce en el canal que se presenta en la siguiente figura. El canal cuenta con una estructura tipo 'pila' la cual interrumpe el flujo.
# 
# Considere que la velocidad inicial es de u=1.5m/s y v=0m/s para todo el dominio. Las condiciones de contorno son las siguientes:
# 
# $u, v, p$ son periódicas en $x=0,20$
# 
# $u, v =0$ en $y =0,4$. Pared del canal
# 
# En este punto, considere que dentro de la 'pila' la velocidades son cero tanto para u como v. Imponga esto.
# 
# $\frac{\partial p}{\partial y}=0$ at $y =0,4$
# 
# Para la definición del delta de tiempo considere una condición de CFL< 0.1. 
# 
# 2.1) Grafique en planta el campo de velocidades 2D que se presenta. Se recomienda que use plotly para ello.
# 
# 2.2) Extraiga el perfil de velocidad que se produce a lo largo de Y, en un 5%, 50% y 95% de la longitud X (20m). Realice lo mismo con respecto al otro eje.
# 
# 2.3) Que ve que se produce cerca de la pila?. La presencia de la pila afecta el flujo aguas arriba?

# ![IPython](images/tp4_2.png)

# $\bf{Ejercicio 3}$.Aplique el code de Navier-Stokes 2D suponiendo que tiene que evaluar y describir el flujo que se produce en el canal que se presenta en la siguiente figura.  En este caso se colocó un 'espigon' para desplazar el flujo. En este caso considere que la velocidad inicial es de u=0m/s y v=0m/s para todo el dominio. Las condiciones de contorno son las siguientes:
# 
# $u, v, p$ son periódicas en $x=0,20$
# 
# $u, v =0$ en $y =0,4$ Pared del canal
# 
# $\frac{\partial p}{\partial y}=0$ at $y =0,4$
# 
# Considere una condición de CFL< 0.1. 
# 
# 3.1) Extraiga el perfil de velocidad sobre Y para los mismo puntos indicados anteriormente
# 
# 3.2) Describa el flujo que ve en el canal.
# 
# 3.3) Modifique el paso de tiempo. Siempre verificando la condición de CFL. Describa lo que ve
# 
# Opcional: 3.3) Hay conservación de masa del flujo?

# ![IPython](images/tp4-3.png)

# In[ ]:




