# Consistencia, Convergencia y Estabilidad

# Required modules
import numpy as np
import matplotlib.pyplot as plt

import nbtools as nb

Definimos el factor de amplificación para el esquema de **advección explícita de primer orden**

def ampf_advection_explicit(sigma, kmdx):
    amp = np.abs(1-sigma+sigma*np.exp(-1j*kmdx))
    return amp

Luego, definimos los números CFL ``sigmas`` y los números de onda por el espaciado de la cuadrícula ``kmdx``

sigma_start = 0.5
sigma_end = 1.0

sigmas = np.linspace(sigma_start, sigma_end, 6)
kmdx = np.linspace(0, np.pi, 100)

Generamos los gráficos ejecutando la siguiente celda 

# Initialize plot and colors

fig, ax = plt.subplots()
colors = ['r','y','k','m','g','b']

for sigma, color in zip(sigmas, colors):
    amp = ampf_advection_explicit(sigma, kmdx)
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.1f}$')

#muestro leyenda
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

#cambiamos manualmente el xticks 
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$']);

Ahora pruebo el factor de amplificación para el esquema de advección implícito

def ampf_advection_implicit(sigma, kmdx):
    amp = np.abs(1/(1-sigma+sigma*np.exp(-1j*kmdx)))
    return amp
   

Dado que este esquema es incondicionalmente estable, mostramos los resultados para valores más grandes de ``sigma``. Genere curvas para seis valores equiespaciados de $ \sigma \in [0,10] $, siguiendo el código de la segunda celda.

# Generate new values of sigma here
sigma_start = 0 
sigma_end = 10

sigmas = np.linspace(sigma_start, sigma_end, 6)
kmdx = np.linspace(0, np.pi, 100)


#ploteo
fig, ax = plt.subplots()
colors = ['r','y','k','m','g','b']

for sigma, color in zip(sigmas, colors):
    amp = ampf_advection_implicit(sigma, kmdx)
    ax.plot(kmdx, amp, color=color, label=f'$\sigma={sigma:.0f}$')

#Muestro leyenda
ax.legend()

# Set x and y labels
ax.set_xlabel(r'$\kappa_m \Delta x$')
ax.set_ylabel(r'$|e^{a\Delta t}|$')

#cambiamos manualmente el xticks 
plt.xticks([0, np.pi/4, np.pi/2, 3*np.pi/4, np.pi], 
           ['0', r'$\frac{\pi}{4}$',  r'$\frac{\pi}{2}$',  r'$\frac{3\pi}{4}$', r'$\pi$']);

