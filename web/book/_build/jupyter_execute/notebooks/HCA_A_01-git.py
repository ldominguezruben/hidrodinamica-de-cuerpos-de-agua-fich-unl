#!/usr/bin/env python
# coding: utf-8

# 
# # Git, Gitlab y GitHub

# ## ¿Qué es git?

# Supongamos que trabajan en un proyecto (el PROYECTO TESIS)
# 
#  - Crean el directorio *Proyecto TESIS DOC* para guardar los archivos de código, recursos, etc.
# 
# Ahora su Jefe/Director le pide hacer cambios (uno de posiblemente muchso). Entonces ¿Qué hacen?

#  - Modifican los codigos originales? (Con la esperanza de que el Jefe sabe lo que pide)

#  - Crean el directorio *Proyecto TESIS DOC_v21289304*? (Con la esperanza de que no les pidan 18 cambios más adelante)

# Git es un sistema de control de versiones, que les permite realizar una serie de procesos en su codigo o desarrollo:
#  - Guardar el historial de revisiones de un proyecto en un directorio
#  - Retroceder a cualquier versión del proyecto en cualquier momento
#  - Implementar nuevas características sin romper el codigo principal (Branches)
#  - Facilita la colaboración con varias personas. Permite que cada una de ellas tenga su propia versión de los archivos en su computadora.

# ## Cómo funciona?

#  - Git lleva el registro de cambios del repositorio. El repositorio es un directorio, todos sus archivos y subdirectorios.
#  - Los cambios se registran con **commits**.

# ### Para bajar este repo en su carpeta local
# ``` git clone https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl```
# 
# ### Para actualizarlo 
# ```git pull https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl```

# ## Configuración incial
# ----
# Veamos si lo tenemos instalado

# In[3]:


get_ipython().system('git --version  # La última versión estable es 2.28. ')


# ### Nombre y Correo
!git config --global user.name ldominguezruben
!git config --global user.email ldominguezruben@gmail.com
# In[5]:


get_ipython().system('git config --list    # Verificar la configuracion')


# ## Crear repositorio
# ----
# En el directorio del Proyecto: <code>git init</code>

# In[ ]:


mkdir Proyecto_TESIS_DOC    
cd Proyecto_TESIS_DOC
git init
git add (filename)
git commit -m 'lo que quiero que quede como referencia de la modificacion'
git push 


# ## Deshacer cambios
# -----
# Para volver atras necesitamos primero el **id** del commit al cual queremos volver.
# Usamos <code>git log</code> para ver el historial de commits.

# Hay **tres** formas de volver atras:
# 
# **1)** <code>git checkout **id**</code>: No cambia nada, solo lleva el HEAD a ese commit. Podemos ver los codigos como eran en ese momento. Podemos abrir un nuevo branch. Para volver el HEAD al punto actual: <code>git checkout master</code>. (O el nombre de la rama en la que estemos.)
# 
# **2)** <code>git revert **id**</code>: Inserta un nuevo commit que regresa el estado del codigo a como era en el commit especificado. No se borra nada!

# **3)** <code>git reset **id**</code>: Borra todo el historial de commits posterior al commit especificado. **Warning:** borra solo el historial de commits, no los cambios en los archivos. Todas las modificaciones siguen estando! Se puede volver a hacer un commit con todos los cambios juntos.
# 
# **3.soft)** <code>git reset **id** --soft</code>: Borra historial de commits. Los cambios vuelven al Stage.
# 
# **3.hard)** <code>git reset **id** --hard</code>: Borra historial de commits y cambios en los archivos **permanentemente**.

# ## Merge
# -----
# Supongamos que queremos mergear dos ramas, **RamaA** y **master**. 
# 
# <code>git checkout master</code>: Nos ubicamaos en la rama receptora del merge.
# 
# <code>git merge RamaA</code>: Pedimos el merge de la RamaA. Trae todos los commits de la RamaA. Se puede hacer

#  - Servicio online que hospeda nuestro repositorio de codigo
#  - Compartir codigo con otros desarrolladores
#  - Cualquiera puede descargar el código y modificarlo
#  - Cualquiera puede subir sus cambios y unirlos al código

# ## Crear Repositorio Remoto
# -----
# ### Opción A. Ya tenemos un Repo Local y lo queremos en GitHub
# Creamos un nuevo repo en GitHub con **el mismo nombre** que el repositorio Local. Luego:
# 
# <code>git remote add origin https://gitlab.com/USUARIO/NOMBRE_REPO</code>
# <br/>
# <code>git push origin master</code>
# 
# 
# 
# ### Opción B. Creamos primero el Repo Remoto y lo clonamos localmente. (ESTO ES LO QUE TENEMOS NOSOTRS)
# 
# <code>git clone https://gitlab.com/USUARIO/NOMBRE_REPO</code>

# ## Sincronizar Local y Remoto
# -----
# 
# Ahora podemos introducir cambios en los archivos localmente y sincronizar con el remoto:
# <br/>
# <code>git push origin master</code>
# 
# Si trabajan en equipo probablemente se la pasen trabajando en alguna rama:
# <br/>
# <code>git push origin NOMBRE_RAMA</code>
# 
# Si algun colaborador hizo cambios en el remoto, lo sincronizan con el local:
# <br/>
# <code>git pull origin NOMBRE_RAMA</code>
