# Time-Stepping Methods

In this notebook, we will explore different methods to solve ordinary differential equations. Specifically, we will implement some explicit and implicit methods to advance our spatial discretizations schemes in time.

To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.

To get started, import the required Python modules by running the cell below.

# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb

In order to advance the solution of our partial differential equations in time, we generally write it in the form
$$ \frac{du}{dt} = R(u),$$
where $R$ is generally known as the _residual_. For example, in the case of the linear advection equation, it can be written
$$ R = -\alpha\frac{\partial u}{\partial x}.$$
Consider the second-order central advection scheme
$$ R = -\alpha\frac{u_{i+1} - u_{i-1}}{2\Delta x}.$$

Run the cell containing the function ``residual``, which corresponds to the above equation of $R$ with periodic boundary conditions applied.

def residual(u, a, dx):
    # Second-order accurate central method
    return -0.5*a/dx*(np.roll(u, -1) - np.roll(u, 1))

Now, let's define our computational grid. For this, consider a domain of length ``L=1.0`` with ``n=100`` nodes.

L = 
n = 

dx = L/n
x = np.linspace(0, L-dx, n)

We also pre-define the final time ``tf=1.0``, the advection speed ``a=1.0`` and an initial condition 

$$u(x,0) = e^{-40\left(x-\frac{1}{2}\right)^2}$$

tf = 
a = 

# Initialize solution
u0 = np.exp(-40*(x-1/2)**2)

## The Midpoint Method
The second-order midpoint method can be written as two stages, i.e
1.  $~\tilde u = u^t + \frac{\Delta t}{2} R(u)$

2.  $~u^{t+1} = u^t + \Delta t R(\tilde u)$

The function ``midpoint_method`` implements this scheme.

def midpoint_method(ut, a, dx, dt, tf):
    t = 0
    while t < tf:
        # Compute residual R(u)
        r = residual(ut, a, dx)
        
        # Compute solution ũ at dt/2
        um = ut + 0.5*dt*r
        
        # Compute residual corresponding to ũ
        rm = residual(um, a, dx)
        
        # Computer solution at t+Δt
        uf = ut + 1.0*dt*rm
        
        ut = uf
        t += dt 
    return ut

Run the advection case using the midpoint method and a time-step size of ``dt=0.005``

dt = 0.005
uf = midpoint_method(u0, a, dx, dt, tf)
plt.figure()
plt.plot(x, uf, label="Midpoint Method")
plt.legend()

Now, run the simulation until ``tf=6.0``. What behaviour do you observe?

## The RK4 method

In the next cell, rewrite the residual function using a fourth-order accurate spatial stencil

def residual(u, a, dx):
    # Implement fourth-order stencil
    # ...

Similar to the Midpoint method, write a function that advances the solution in time using the fourth-order four-stage Runge-Kutta method. Refer to the textbook for the corresponding Butcher tableau.

def rk4(ut, a, dx, dt, tf):
    t = 0
    while t < tf:
        # Implement RK4 method
        # ...
        ut = uf
        t += dt 
    return ut

Using a time-step size ``t=0.01``, run the simulation until ``tf=1.0``.

dt = 
uf = rk4(u0, a, dx, dt, tf)
plt.figure()
plt.plot(x, uf, label="RK4")
plt.legend()

Run the fourth-order spatial scheme using both time-stepping methods and``dt=0.01, 0.005`` until times ``tf=1.0`` and ``tf=2.0``. Comment on the results.