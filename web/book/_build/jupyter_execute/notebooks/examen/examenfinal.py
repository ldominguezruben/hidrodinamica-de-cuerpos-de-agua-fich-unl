# Examen Final. Hidrodinámica de Cuerpos de Agua
## FICH-UNL. 

Aclaraciones:
- El examen se entrega y presenta el día 7 de Julio a las 1030AM y deben enviar el examen final antes de las 1030AM del día 8 de julio. Su duración total es de 24hs
- Revisar el examen antes de entregar (no se permitiran dos envios).
- Se puede presentar en formato pdf o ipynb
- DUDAS?

## Ejercicios

$\bf{Ejercicio 1}$. Represente el patrón de velocidades que se produce en un fluído en una cavidad para las condiciones que se presentan en la siguiente figura:

![IPython](fig1.png)

La condición inicial es $u_x, u_y, p=0$ en todos los puntos y las condiciones de contorno son:

$u_x=2$m/s en y=4

$u_x,u_y=0$ en las otras fronteras

$\frac{\partial p}{\partial y}=0$ en $y=0$

$p=0$ en $y=4$

$\frac{\partial p}{\partial x}=0$ en $x=0, 4$

Resuelva la ecuación de Navier-Stokes acorde al caso presentado, discretice y obtenga el patrón de velocidades que se desarrollan. Utilice el método de diferencias finitas para ello. Defina el paso de tiempo en función de la condición de CFL<0.5 y determine el número de nodos adecuado para una buena representación del fenóneno.
- Presente el código en Python. Detalle y comente correctamente el mismo
- Determine el número de pasos de tiempo que logren la estabilidad del fenómeno
- Grafique en planta con una función tipo contourf, junto a los vectores asociados.
- ¿Que tipo de flujo presenta el problema?. 
- Explique la interacción de fenómeno de advección y difusión en el patrón final que encuentra con relación al dominio geométrico modelado.
- Extienda el dominio considerando ahora $L_x=30m$ ($L_y=4m$). ¿Qué observa en este caso?.
- En base a este último resultado, ¿puede predecir el comportamiento si se continúa extendiendo el dominio en $x$?

$\bf{Ejercicio 2}$. Proponga un esquema iterativo para calcular el tirante de equilibrio dado la descarga líquida, pendiente y el tipo de sedimento (rugosidad del lecho). Usando la siguiente expresión de Keulegan:

$$\frac{U}{U*}=\frac{1}{\sqrt{C_f}}=\frac{1}{\kappa}\ln\left(\frac{11H}{K_s}\right)$$

donde $K_s=n_k D$, con la relación que vincula la velocidad de corte ($U_*$), el gradiente hidráulico ($S_w$) y constante de von Karman ($\kappa$):

$$u_*=\sqrt{gHS_w}; S_w=-\frac{dZ_w}{dX}$$

Siendo $U_*=\sqrt{\frac{\tau_b}{\rho}}$ y $\tau_b =\rho C_f U^2$, válido bajo la hipótesis de canal ancho ($H/B\ll 1$), mas la restricción de conservación de la masa $Q=U\Omega$

![IPython](fig2.png)

* (a) Considere un canal rectangular (Figura a) que presenta una pendiente longitudinal uniforme de $S=S_w=0.002$, dos anchos diferentes, 39.1cm y 60.4cm para las descargas líquidas de 6l/s y 8l/s, respectivamente. Suponga ademas un diámetro medio $D_{50}=0.94mm$. Calcule para ambas condiciones de caudales el tirante de equilibrio esperado utilizando un rango de $n_K=[1-3]$.
* (b) Modifique el esquema a fin de adquirir una sección trapezoidal (Figura b) en los calculo, manteniendo la hipótesis de canal ancho ($H/B\ll 1$), para un talud lateral $z=1.7:1 $ (H:V). Repita los calculos.
* (c) Proceda ahora a la inversa, manteniendo esta última sección trapezoidal en los cálculos. Si $H$=3.5cm y 3.3cm fueron los tirante medidos en ambos ensayos para las descargas $Q$=6 l/s y 8 l/s, respectivamente, con discrepancias estimadas en el orden de $\pm 0.2cm$. ¿Cuál es la rugosidad aparente actuante durante los ensayos?. Es decir, calcule los valores efectivos de $n_K$ que reproducen los valores observados de $H$.

Tabla 1.- Resumen con los parámetros geométricos e hidráulicos.

| $Q$[m3/s]| B[m] | $z$ | H_{obs} [m]| 
| :---: | :---: | :---: | :---: | 
| 0.006 |  0.391 | 1.7 | 0.035|
| 0.008 |  0.604 | 1.7 | 0.033|


$\bf{Ejercicio 3}$. Considere las mediciones llevadas a cabo en un cauce natural representadas en la siguiente figura que muestran el perfil de velocidad streamwise

![IPython](fig3.png)

Calcule
* (a) velocidad de corte ($U^*$)
* (b) tensión de pared $\tau_b$
* (c) espesor de la subcapa laminar $\delta=11.6\nu/U_*$. Siendo $\nu=10^{-6} m^2/s$
* (d) ¿Qué condiciones se deben verificar para considerar al lecho como hidrodinámicamente liso o rugoso?

