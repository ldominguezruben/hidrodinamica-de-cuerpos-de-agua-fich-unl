
# Diseño de software para cómputo científico

----

## Unidad 1: Librerías de cómputo científico en Python



### Agenda de la Unidad 1
---

 - Introducción al lenguaje Python.
 - **Librerías de cómputo científico.**

### Librerías de cómputo científico

----

**Python cientific stack**

- Numpy + Scipy + Matplotlib

**Newcomers**

- Pandas
- Bokeh

**Alto nivel y proposito especifico**

- Holoviews
- Scikit-algo
- *muchas otras*

![image.png](attachment:image.png)

Copiado de: https://studylib.es/doc/8227271/presentaci%C3%B3n---facundo-batista


### Numpy

----

**¿Qué es NumPy?**

- Es una biblioteca de Python para trabajar con arreglos multidimensionales.
- El principal tipo de dato es el arreglo o array. También nos permite trabajar con la semántica de matrices
- Nos ofrece muchas funciones útiles para el procesamiento de numérico
- Los principales métodos están compilados en C.


**Pueden ver toda la info de esta presentación en** 
- https://numpy.org/doc/stable/user/quickstart.html
- Los ejemplos suponen que primero se hizo en el intérprete:

import numpy as np

### El Array multidimencional 
----

- Es un arreglo 
- Todos del mismo tipo
- Indexados por enteros

**Ejemplo de arreglos multidimensionales**

- Vectores, Matrices, Imágenes o Planillas

**¿Multidimensionales?**

- Que tiene muchas dimensiones o ejes
- Un poco ambiguo, mejor usar **ejes**
- Rango: cantidad de ejes

### Propiedades de los Array

---

Como tipo de dato se llama `ndarray`

- `ndarray.ndim`: cantidad de ejes
- `ndarray.shape`: una tupla indicando el tamaño del array en cada eje.
- `ndarray.size`: la cantidad de elementos en el array.
- `ndarray.dtype`: el tipo de elemento que el array contiene.
- `ndarray.itemsize`: el tamaño de cada elemento en el array.


### Propiedades de los Array
---

a = np.arange(10).reshape(2,5)
a

a.shape

a.ndim

a.size

a.dtype

a.itemsize

### Creando Arrays
----

#### Usando otro iterable como argumento

np.array([2,3,4], dtype=np.float64)

np.array([[1.5,2,3], [4,5,6]])


### Creando Arrays
----
#### Con funciones específicas en función del contenido

np.arange(5)

np.zeros((2, 3))

np.zeros((1, 3), dtype=bool)

np.ones((3, 2), dtype=bool)

np.empty((2, 2))

np.linspace(-np.pi, np.pi, 5)

### Manejando ejes del Array
----

a = np.arange(6)
a

a.shape = (2, 3)
np.shape?

a.shape = (3, 72)
a

a.size

### Operaciones básicas
----

**Los operadores aritméticos se aplican por elemento**

a = np.arange(20, 60, 10)
a

b = a + 1
b

a * 2

b is a, id(a), id(b)

**Si es *inplace*, no se crea otro array**

a += 2
a

### Operaciones básicas

----

**Podemos realizar comparaciones**

a = np.arange(5)
a >= 3

a  % 2 == 0

**También con otros arrays**

a, b = np.arange(20, 60, 10), np.arange(4)
a, b

a - b

a * b

### Operaciones básicas
---
**Tenemos algunos métodos con cálculos típicos**

c = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).reshape((2,5))
c.min(), c.max(axis=0)
np.argmin(c)

print(c)
c.mean(), c.sum(axis=0)

c.cumsum()

Hay muchas funciones que nos dan info del array

```all, alltrue, any, argmax, argmin, argsort, average, bincount, ceil, clip,conj, conjugate, corrcoef, cov, cross, cumprod, cumsum, diff, dot, floor,inner, inv, lexsort, max, maximum, mean, median, min, minimum,nonzero, outer, prod, re, round, sometrue, sort, std, sum, trace,transpose, var, vdot, vectorize, where...```

### Trabajando con los elementos
----

La misma sintaxis de Python

a = -np.arange(10)
a

a[2]

a[2: 5]

a[1] = 88

a[-5:] = 100
a

a[3,:]



### Trabajando con los elementos
----

Pero también podemos trabajar por eje

a = np.arange(8).reshape((2,4))
a

a[1,0]

b = a[0,-2:]   # b apunta al arreglo a
print(b)

b[0] = 100     # si modificas b, modificas a
print(b)

a

### Cambiando la forma del array

----

Transponer y aplanar

a = np.arange(8).reshape((2,4))
a

a.transpose() # a.T

a.flatten()  # a.ravel() cuidado!

### Juntando y separando arrays
----

Tenemos vstack y hstack

a = np.ones((2,5))
a

b = np.arange(5)
b

np.vstack((a,b,a))

np.hstack(([1, 2, 3], [4, 5, 6]))

### Juntando y separando arrays
----

También hsplit y vsplit

juntos = np.array([
    [ 1.,  1.,  1.,  1.,  1.],
    [ 1.,  1.,  1.,  1.,  1.],
    [ 0.,  1.,  2.,  3.,  4.]])

for l in np.hsplit(juntos, (1,3)):
    print(l)
    print("-" * 10)

### Indexado avanzado
----
Podemos indexar con otros arrays

a = np.arange(10) ** 2
a

a[[1, 4]]

i = np.array([(2, 3), (6, 7)])
a[i]

### Indexado avanzado
----

o elegir elementos

a = np.arange(5) ** 2
a

b = (a % 2 == 0)
~b

a[~b]

o verificar donde se cumple alguna condición

np.argwhere(a  % 2 == 0)

### Ordenar arreglos
Podemos ordenar los elementos

a = np.array([2, 3, -5, 0, 2])
np.argsort(a)

# Para 2 dimensiones hay que especificar el eje. Por defecto es -1 (el último).
a = np.array([[2, 3], [7, 5], [-3, -6]])
ind = np.argsort(a, axis=1)

#Para recuperar el arreglo ordenado por fila (axis=1)
np.take_along_axis(a, ind, axis=1) 

### Matrices
----
Es un caso específico del array

a = np.arange(6).reshape((2, 3))
a

A = np.matrix(a)
A

A.T

A.I

### Matrices
--------

Obviamente tienen semática de matrices

A

M = np.matrix([[2, 3],[4, 5],[6, 7]])
M

A * M

A @ M

np.array(A) @ np.array(M)

#### Polinomios
----

p = np.poly1d([2, 3, 4])
print(p)

print(p * p)

print(p.integ())

p([1, 10, 100, 1000])

### Lectura y escritura de arreglos

a = np.random.randint(10, 10000, size=50).reshape((10, 5))
a

np.savetxt('enteros.dat', a)     # mejor usar np.save() y np.load()
b = np.loadtxt('enteros.dat')
b

### Notas finales sobre numpy

---

Casi todo lo que se puede hacer con `nd.array` se puede hacer con listas mas una funcion de numpy

-np.cumsum([1, 2, 3])

- Se puede crear array desde generadores usando `np.fromiter`

def gen():
    yield 1
    yield 2
    yield 100
    
np.fromiter(gen(), dtype=int)

- Si van a crear y cargar un array usen SIEMPRE np.empty()
- La idea de los `np.ndarray` es evitar a TODA costa el `for` loop.


## Scipy

![image.png](attachment:image.png)

Continua la copia de la presentación anterior

### Scipy

----

**¿Qué es Scipy?**

- Colección de algoritmos matemáticos y funciones.
- Construido sobre NumPy.
- Agrega mucho al análisis interactivo.
- Procesamiento de datos y prototipado de sistemas.
- Compite con Matlab, IDL, Octave, R-Lab, y SciLab.

**Osea:** Son un monton de funciones que se aprovechan de Numpy

### Integrations
---- 
- General integration.
- Gaussian quadrature.
- Integrating using samples.
- Ordinary differential equations.

from scipy import integrate
from scipy import special

def J2(x):
    '''Función de Bessel J2'''
    return special.jv(2, x)

result = integrate.quad(J2, 0, 4.5)
result

### Optimizaciones
---

- Nelder-Mead.
- Simplex algorithm.
- Broyden-Fletcher-Goldfarb-Shanno algorithm.
- Newton-Conjugate-Gradient.
- Least-square fitting.
- Scalar function minimizers.
- Root finding

from scipy import optimize

def f(x):
    return (x**3 - 1)  # only one real root at x = 1

sol = optimize.root_scalar(f, bracket=[0, 3], method='brentq')
print(sol)
print(f'Raíz: {f(sol.root)}')

### Interpolaciones 
---

- Linear 1-d interpolation
- Spline interpolation in 1-dI
- Two-dimensional spline representation

![image.png](attachment:image.png)

### Signal Processing

----

- B-splines
    - second- and third-order cubic spline coefficients
    - from equally spaced samples in one- and two-dimensions
- Filtering
    - Convolution/Correlation
    - Difference-equation filtering-
    - Other filters: Median, Order, Wiener, Hilbert, ...

![image.png](attachment:image.png)

### Algebra lineal
---

- Matrices
    - Inversas
    - Determinantes
    - Resolución de sistemas lineales
- Descomposiciones
    - Eigenvalues and eigenvectors
    - Singular value, LU, Cholesky, QR, Schur
- Funciones de matrices
    - Exponentes y logaritmos
    - Trigonometría (común e hiperbólica)

### Estadísticas
---
- Masked statistics functions: 64!
- Continuous distributions: 81!
- Discrete distributions: 12!
- Statistical functions: 72!

![image.png](attachment:image.png)

Fuente: https://www.datacamp.com/community/tutorials/matplotlib-tutorial-python

### Matplotlib 

- Es una biblioteca para la generación de gráficos a partir de datos contenidos en listas o arrays en el lenguaje de programación Python y su extensión matemática NumPy. 
- Orientado a publicaciones.
- Proporciona una API:
    1. `pylab`, diseñada para recordar a la de MATLAB (NO LA USEN).
    2. `pyplot`, diseñada para ser extendida.
    
 

import matplotlib.pyplot as plt

#%matplotlib inline

### Ejemplo Matlab

# Prepare the data
x = np.linspace(0, 10, 100)

# Plot the data
plt.plot(x, x**3, label='linear')

# Add a legend
plt.legend();

- El código anterior simplemente se usaron shortcuts para utilizar la estructura subjacente de `pyplot`.
- In essence, there are two big components that you need to take into account:

    1. La `Figure` un contenedor que contiene cosas "dibujables".
    2. Los `Axes` es el area donde los datos son ploteados con funciones como `plot()` o `scatter()`. Estos son lo elementos que pueden tener *ticks*, *labels*, etc. 

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot([1, 2, 3, 4], [10, 20, 25, 30], color='lightblue', linewidth=3)
ax.scatter([0.3, 3.8, 1.2, 2.5], [11, 25, 9, 26], color='darkgreen', marker='^')

ax.set_xlim(0.5, 4.5)
plt.show()

### Python Visualization overview
----

![image.png](attachment:image.png)

Fuente: https://pyviz.org

### Cuales valen la pena ver? - Seaborn

- Deja mas "bonitos" los graficos de `matplotlib`.
- Trae muchos mas gráficos útiles.
- Integrado muy bien con **Pandas**.

import seaborn as sns; sns.set()

fig = plt.figure()
ax = fig.add_subplot(111)

ax.plot([1, 2, 3, 4], [10, 20, 25, 30], color='lightblue', linewidth=3)
ax.scatter([0.3, 3.8, 1.2, 2.5], [11, 25, 9, 26], color='darkgreen', marker='^')

ax.set_xlim(0.5, 4.5)
plt.show()

### Cuales valen la pena ver? - Bokeh

- Bokeh es una librería de visualización interactiva en `HTML`.
- Contempla su publicación en papel (pero no tan bien como matplotlib)

from bokeh.plotting import figure, show, output_notebook
from bokeh.sampledata.iris import flowers
output_notebook()

colormap = {'setosa': 'red', 'versicolor': 'green', 'virginica': 'blue'}
colors = [colormap[x] for x in flowers['species']]

p = figure(title="Iris Morphology", width=320, height=240)
p.circle(flowers["petal_length"], flowers["petal_width"],
         color=colors, fill_alpha=0.2, size=10)
show(p)

### Cerrando visualización
---

- [Opinión personal] Mi trabajo lo hago con matplotlib. Seaborn suma con estética pero es lento.
- Hay un proyecto que super interesante que se llama Holoviews que integra bokeh + matplotlib.
- Trabaja en base a darle anotaciones a los datos (cargarlos de semantica) https://www.youtube.com/watch?v=aZ1G_Q7ovmc
- Hay proyectos espécificos como `YT` orientado a simulaciones cosmologias (por ejemplo)

### Cerrando computo científico
----

![image.png](attachment:image.png)

