#!/usr/bin/env python
# coding: utf-8

# # Tipos de datos

# ## Tipos de variables básicas
# 
# En Python, hay algunos tipos de datos que necesitamos conocer, ya que los valores numéricos, int, float y complex son los tipos asociados con los valores. 
# 
# * **int**: Entero, tales como 1, 2, 3, ...
# * **float**: Numeros Flotantes, tales como 3.2, 6.4, ...
# * **complex**: Numeros complejos, tales como 2 + 5j, 3 + 2j, ...
# 
# Podemos usar el metodo *type* para chequear el tipo de valor.

# In[8]:


type("Lucas")


# In[3]:


type(2.5)


# In[4]:


type(2 + 5j)


# In[ ]:


get_ipython().run_line_magic('whos', '')


# ## Cadenas (Strings)

# Existen diferentes tipos de datos, como int, float y boolean, todos ellos relacionados con un solo valor. En esta sección se presentarán más tipos de datos para que podamos almacenar varios valores. La estructura de datos relacionada con estos nuevos tipos son cadenas, listas, tuplas, conjuntos y diccionarios.
# 

# Una cadena es una secuencia de caracteres, como "Una IPA, por favor" que vimos previamente. Las cadenas están rodeadas por comillas simples o dobles. Podríamos usar la función *print* para enviar las cadenas a la pantalla.
# 

# In[9]:


print("Una IPA, por favor")


# Asignamos una variable a la cadena previa

# In[10]:


w = "Una IPA, por favor"


# In[12]:


type(w)


# Tenga en cuenta que un espacio en blanco, " ", entre "Una" y "IPA" también es un tipo *str*. Cualquier símbolo puede ser un carácter, incluso los que se han reservado para los operadores. Tenga en cuenta que como *str*, no realizan la misma función. Aunque tienen el mismo aspecto, Python los interpreta de manera completamente diferente.

# Una cadena es una matriz de caracteres, por lo tanto, tiene una longitud para indicar el tamaño de la cadena. Por ejemplo, podríamos verificar el tamaño de la cadena usando la función incorporada *len*.

# In[13]:


len(w)


# Las cadenas también tienen índices para indicar la ubicación de cada carácter, de modo que podamos encontrar fácilmente algún carácter. El índice de la posición comienza con 0, como se muestra en la siguiente imagen.
# 
# | Cadena | U |  N | A |   | I | P | A | , |   | P | O  | R  |    | F  | A  | V  | O  | R  | 
# | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
# | Indice | 0 |  1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 
# 

# Podríamos tener acceso a cualquier carácter usando un corchete y el índice de la posición. Por ejemplo, si queremos obtener el carácter 'W', entonces tenemos que hacer:

# In[25]:


w[5:]


# También podríamos seleccionar una secuencia. Por ejemplo, si queremos obtener el "IPA", podríamos hacer el siguiente comando.

# In[ ]:


w[4:7]


# [4:7] significa que la posición de inicio es del índice 4 y la posición final es el índice 6. Para el rango de corte de cadenas de Python, el límite superior es exclusivo, lo que significa que [4:7] en realidad es para cortar los caracteres de 4 -> 6. La sintaxis para cortar en Python es [inicio: final: paso], el tercer paso es opcional.
# 

# Tambien se puede

# In[ ]:


w[9:]


# In[ ]:


w[:7]


# También puede usar un índice negativo al cortar las cadenas, lo que significa contar desde el final de la cadena. Por ejemplo, -1 significa el último carácter, -2 significa el segundo al último y así sucesivamente.

# In[ ]:


w[9:-6]


# In[ ]:


w[::3]


# Las cadenas no se pueden usar en las operaciones matematicas

# In[ ]:


1 "+" 2


# Podemos concatenar caracteres:

# In[ ]:


str_a = "Amo Python! "
str_b = "Demasiado!"

print(str_a + str_b)


# En Python, cadena como un objeto que tiene varios métodos que podrían usarse para manipularlo (hablaremos más sobre programación orientada a objetos más adelante). La forma de obtener acceso a los distintos métodos es utilizar este patrón "string.method_name".. 
# 

# In[ ]:


w.upper()


# In[ ]:


w.count("U")


# Usamos elmetodo de remplazo.

# In[ ]:


w.replace("IPA", "APA")


# Hay diferentes formas de preformatear una cadena. Aquí presentamos dos formas de hacerlo. Por ejemplo, si tenemos dos variables *name* y *country*, y queremos imprimirlas en una oración, pero no queremos usar la concatenación de cadenas que usamos antes, ya que usará muchos signos '+'. En su lugar, podríamos hacer lo siguiente: 

# In[ ]:


name = "UNL"
country = 'Argentina'

print("%s es un gran universidad de %s!"%(name, country))


# In[ ]:


print(f"{name} es un gran universidad de {country}.")


# ## Listas

# Veamos una estructura de datos secuenciales más versátil en Python - Listas. La forma de definirlo es usar un par de corchetes [ ], y los elementos dentro de él están separados por comas. Una lista puede contener cualquier tipo de datos: numéricos, cadenas u otros tipos. Por ejemplo:

# In[29]:


list_1 = [1, 2, 3, 'Hello']
list_2 = ['World']


# In[30]:


list_3 = [list_1, list_2]
list_3


# Elementos de una lista

# In[31]:


list_3[1]


# Podemos saber la longitud de una lista aplicando *len*

# In[32]:


len(list_3)


# Podemos sumas elementos a una lista usando la clave *append*

# In[33]:


list_1.append(4)
list_1


# **Note!** La función *append* opera en la lista como se muestra en el ejemplo anterior, se agrega 4 a la lista. Pero en el ejemplo list_1 + list_2, list_1 y list_2 no cambiarán. Puede comprobar list_2 para verificar esto.

# In[34]:


list_1.insert(2,'medio')
list_1


# **Note!** El uso del método *remove* solo eliminará la primera aparición del elemento (lea la documentación del método). Hay otra forma de eliminar un elemento usando su índice - función *del*. 

# In[35]:


del list_1[1]
list_1


# Usando la función *list*, podríamos convertir otros elementos de secuencia en una lista.

# In[36]:


list('Hello World')


# ## Tuplas

# Por lo general, se define mediante un par de paréntesis ( ) y sus elementos están separados por comas. Por ejemplo:

# In[39]:


tupla_1 = (1, 2, 3, 2, 'medio')
tupla_1


# Como cadenas y listas, la forma de indexar las tuplas son muy similares.

# In[ ]:


len(tupla_1)


# In[38]:


tupla_1[1:4]


# Contamos la ocurrencia de un numero en la tupla

# In[40]:


tupla_1.count(2)


# Puede preguntar, ¿cuál es la diferencia entre listas y tuplas? Si son similares entre sí, ¿por qué necesitamos otra estructura de datos de secuencia?
# 
# Bueno, las tuplas se crean por una razón. De la [documentación de Python] (https://docs.python.org/3/tutorial/datastructures.html#tuples-and-sequences):
# 
# > Aunque las tuplas pueden parecer similares a las listas, a menudo se usan en diferentes situaciones y para diferentes propósitos. Las tuplas son **inmutables** y, por lo general, contienen una secuencia **heterogénea** de elementos a los que se accede mediante desempaquetado o indexación (o incluso por atributo en el caso de tuplas con nombre). Las listas son **mutables** y sus elementos suelen ser **homogéneos** y se accede a ellos iterando sobre la lista.
# 
# ¿Qué significa inmutable? Significa que los elementos de la tupla, una vez definidos, no se pueden cambiar. Pero los elementos de una lista se pueden cambiar sin ningún problema. Por ejemplo:

# In[41]:


list_1 = [1, 2, 3]
list_1[2] = 1
list_1


# In[42]:


tupla_1[2] = 1


# ¿Qué significa heterogéneo? Las tuplas suelen contener una secuencia heterogénea de elementos, mientras que las listas suelen contener una secuencia homogénea. Veamos un ejemplo, que tenemos una lista que contiene diferentes frutas. Por lo general, el nombre de las frutas podría almacenarse en una lista, ya que son homogéneas. Ahora queremos tener una estructura de datos para almacenar cuántas frutas tenemos para cada tipo, aquí suele ser donde entran las tuplas, ya que el nombre de la fruta y el número son heterogéneos. Como ('manzana', 3) lo que significa que tenemos 3 manzanas.

# In[ ]:


# Frutas
['apple', 'banana', 'orange', 'pear']


# In[ ]:


# Frutas y numeros
[('apple', 3), ('banana', 4) , ('orange', 1), ('pear', 4)]


# Se puede acceder a las tuplas desempaquetando, requiere que el número de variables en el lado izquierdo del signo igual sea igual al número de elementos en la lista.

# In[ ]:


a, b, c = list_1
print(a, b, c)


# **NOTE!** La operación opuesta al desempaque es empacar como se muestra en el siguiente ejemplo. Podríamos ver que no necesitamos los paréntesis para definir una tupla, pero siempre es bueno tener eso.

# In[ ]:


list_2 = 2, 4, 5
list_2


# ## Conjuntos (Sets)

# Otro tipo de datos en Python son los conjuntos. Es un tipo que podría almacenar una colección desordenada sin elementos duplicados. También es compatible con las operaciones matemáticas como unión, intersección, diferencia y diferencia simétrica. Se define mediante un par de llaves { } y sus elementos están separados por comas.

# In[ ]:


{3, 3, 2, 3, 1, 4, 5, 6, 4, 2}


# Un uso rápido de esto es encontrar los elementos únicos en una cadena, lista o tupla.

# In[51]:


set_1 = set([1, 2, 2, 3, 2, 1, 2,8])
set_1


# In[44]:


set_2 = set((2, 4, 6, 5, 2))
set_2


# Aplicamos operaciones como union, intersección a una serie de sets.

# In[45]:


print(set_1)
print(set_2)


# In[46]:


set_1.union(set_2)


# In[47]:


set_1.intersection(set_2)


# In[52]:


set_1.issubset({1, 2, 3, 3, 4})


# ## Diccionarios (Dictionaries)

# Ahora presentaremos un tipo nuevo y útil de estructura de datos: los diccionarios. Es un tipo de mapeo, lo que lo convierte en un tipo totalmente diferente a los que hablamos antes. En lugar de utilizar una secuencia de números para indexar los elementos (como listas o tuplas), los diccionarios se indexan por claves, que pueden ser una cadena, un número o incluso una tupla (pero no una lista). Un diccionario es un par clave-valor, y cada clave se asigna a un valor correspondiente. Se define mediante el uso de un par de llaves { }, mientras que los elementos son una lista de pares clave: valor separados por comas (observe que el par clave: valor está separado por dos puntos).

# In[53]:


dict_1 = {'apple':3, 'orange':4, 'pear':2}
dict_1


# Dentro de un diccionario, los elementos se almacenan sin orden, por lo tanto, no puede acceder a un diccionario basado en una secuencia de números de índice. Para acceder a un diccionario, necesitamos usar la clave del elemento - diccionario [key].

# In[54]:


dict_1['apple']


# Podríamos obtener todas las claves en un diccionario usando el método *keys*, o todos los valores usando el método *values*.

# In[55]:


dict_1.keys()


# In[56]:


dict_1.values()


# Tambien podemos saber la longitud del diccionario usando *len*. 

# In[57]:


len(dict_1)


# Podríamos definir un diccionario vacío y luego completar el elemento más tarde. O podríamos convertir una lista de tuplas con pares (key, value) en un diccionario.

# In[ ]:


school_dict = {}
school_dict['UNL'] = 'Argentina'
school_dict


# Agregamos otra universidad

# In[ ]:


school_dict['Oxford'] = 'UK'
school_dict


# In[ ]:


dict([("UNL", "Argentina"), ('Oxford', 'UK')])


# Podemos chequear si un elemento se encuentra en el diccionario 

# In[ ]:


"UNL" in school_dict


# Podemos probar que otra universidad no se encuentra en el diccionario.

# In[ ]:


"Harvard" not in school_dict


# Tambien podemos listar un diccionario

# In[ ]:


list(school_dict)


# ## Booleanos
# Al igual que en otros lenguajes de programación, en Python existe el tipo bool o booleano. Es un tipo de dato que permite almacenar dos valores *True* o *False*.

# In[ ]:


# Se puede declarar una variable como booleana
x = True
y = False


# In[ ]:


# Evaluar expresiones
print(1 > 0)  
print(1 <= 0) 
print(9 == 9)

