# Resistencia Hidráulica

Descargar la bibliografía de [Keulegan (1923)](https://gitlab.com/ldominguezruben/hidrodinamica-de-cuerpos-de-agua-fich-unl/-/blob/master/web/book/notebooks/ref/jresv21n6p707_A1b.pdf)

<iframe src="ref/jresv21n6p707_A1b.pdf" width="80%" height="400px" frameBorder="0"> </iframe>

## Ley de la pared

#### Para superficies suaves

$$m=\frac{y_0 u_*}{\nu}$$

donde:
*    m: constante
*    $\nu$ viscosidad cinematica

$$ \frac{u}{u_*}=a_s+\frac{1}{\kappa}\ln\left(\frac{yu_*}{\nu}\right)$$ o

$$\frac{u}{u_*}=a_s+\frac{2.3}{\kappa}\ln\left(\frac{yu_*}{\nu}\right)$$

Donde

$$a_s=\frac{1}{\kappa}\ln \left(\frac{1}{m}\right)$$

De los ensayos en laboratorio de Nikuradse:

$$\frac{u}{u_*}=5.05+5.75\log\left(\frac{yu_*}{\nu}\right)$$

#### Para superficies rugosas

![IPython](images/rughness.png)

## Ley de Máximos

$$ \frac{u_{max}-u}{u_*}= 5.75 \log\left(\frac{h}{h-z}\right)$$

Donde:
*   u: es la velocidad en el punto z
*   z: es la distancia medida desde la superficie
*   h: es la profundidad del perfil vertical

## Ley de Manning



