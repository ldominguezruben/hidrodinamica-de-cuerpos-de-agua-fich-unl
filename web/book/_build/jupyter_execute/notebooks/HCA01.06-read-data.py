# TXT Files

So far, we used *print* function to display the data to the screen. But there are many ways to store data onto your disk and share it with other program or colleagues. For example, if I have some strings in this notebook, but I want to use them in another notebook, the easiest way is to store the strings into a text file, and then open it in another notebook. A **text** file, many times with an extension **.txt**, is a file containing only plain text. However, programs you write and programs that read your text file will usually expect the text file to be in a certain format; that is, organized in a specific way.

To work with text files, we need to use *open* function which returns a *file object*. It is commonly used with two arguments: 
```python
f = open(filename, mode) 
```
*f* is the returned file object. The filename is a string where the location of the file you want to open, and the *mode* is another string containing a few characters describing the way in which the file will be used, the common modes are:

* 'r', this is the default mode, which opens a file for reading
* 'w', this mode opens a file for writing, if the file does not exist, it creates a new file. 
* 'a', open a file in append mode, append data to end of file. If the file does not exist, it creates a new file. 
* 'b', open a file in binary mode. 
* 'r+', open a file (do not create) for reading and writing. 
* 'w+', open or create a file for writing and reading, discard existing contents. 
* 'a+', open or create file for reading and writing, and append data to end of file. 

### Write a file

**TRY IT!** Create a text file called *test.txt* and write a couple lines in it. 

f = open('test.txt', 'w')
for i in range(5):
    f.write(f"This is line {i}\n")
    
f.close()

We could see the code above that we first opened a file object *f* with the file name 'test.txt'. We used "w+" for the mode, that indicates write We then write 5 lines (note the newline '\n' at the end of the string), and then we close the file object. We could see the content of the file in the following figure. 

![Write_text](images/11.01.01-Write_text_file.png "The content in the text file we write")

**NOTE!** It is good practice to close the file using `f.close()` in the end. If you do not close them yourself, Python will eventually close them for you. But sometimes, when writing to a file, the data may not written to disk until you close the file. Therefore, the longer you keep the file open, the greater chances you will lose your data. 

### Append to a file

Now, let's append some string to the *test.txt* file. It is very similar to how we write the file, with only one difference - change the mode to 'a' instead. 

f = open('test.txt', 'a')
f.write(f"This is another line\n")
f.close()

![Append_text](images/11.01.02-Append_text_file.png "Append a line to the end of a existing file")

### Read a file

We could read a file from disk and store all the contents to a variable. Let's read in the *test.txt* file we created above and store all the contents in the file to a variable *content*. 

f = open('./test.txt', 'r')
content = f.read()
f.close()
print(content)

Using this way, we could store all the lines in the file into one string variable, we could verify that variable *content* is a string. 

type(content)

But sometimes we want to read in the contents in the files line by line and store it in a list. We could use *f.readlines()* to achieve this. 

f = open('./test.txt', 'r')
contents = f.readlines()
f.close()
print(contents)

type(contents)

### Dealing with numbers and arrays

Since we are working with numerical methods later, and many times, we work with the numbers or arrays. We could use the above methods to save the numbers or arrays to a file and read it back to the memory. But it is not so convenient this way. Instead, commonly we use the *numpy* package to directly save/read an array. Let's see an example. 

**TRY IT!** Store an array [[1.20, 2.20, 3.00], [4.14, 5.65, 6.42]] to a file named *my_array.txt* and read it back to a variable called *my_arr*. 

import numpy as np

arr = np.array([[1.20, 2.20, 3.00], [4.14, 5.65, 6.42]])
arr

np.savetxt('my_arr.txt', arr, fmt='%.2f', header = 'Col1 Col2 Col3')

We can see from the above example to save a 2D array into a text file using *np.savetxt*. The first argument is the file name, second argument is the arr object we wave to save, and the third argument is to define the format for the output (I use '%.2f' to indicate we want the output numbers with 2 decimals). The fourth argument is the header we want to write into the file. 

![Write_array](images/11.01.03-Write_array.png "The numpy array we saved in the file")

my_arr = np.loadtxt('my_arr.txt')
my_arr

We can see read in the file directly to an array is very simple using the *np.loadtxt* function. And it skips the first header as well. There are many different argument that could control the reading, we won't get in too much details here, you can check the documentation or use the question mark to get the help. We will also use it more in the next section in the chapter. 

# CSV Files

There are many scientific data are stored in the **comma-separated values** (CSV) file format, a delimited text file that uses a comma to separate values. It is a very useful format that can store large tables of data (numbers and text) in plain text. Each line (row) in the data is one data record, and each record consists of one or more fields, separated by commas. It also can be opened using Microsoft Excel, and visualize the rows and columns. 

Python has its own csv module that could handle the reading and writing of the csv file, you can see the details in the [documentation](https://docs.python.org/3/library/csv.html). But we are not going to introduce this csv module here. Instead, we will use the numpy package to deal with the csv file since many times we will read csv file directly to a numpy array.   

### Write and open a CSV file

Let's see a simple example of generating 100 rows and 5 columns of data. 

import numpy as np

data = np.random.random((100,5))

np.savetxt('test.csv', data, fmt = '%.2f', delimiter=',', header = 'c1, c2, c3, c4, c5')

We first generate the random data for 100 rows and 5 columns using the *np.random* function and assign it to *data* variable. We use the *np.savetxt* function to save the data to a csv file. We can see that the first 3 arguments are the same for the ones used in the previous section, but here we set the delimiter argument to ',', which indicate that we want to separate the data using comma. 

We can open the csv file using Microsoft Excel. 

![Open_csv](images/11.02.01-Write_csv.png "Open the csv file using Microsoft Excel")

We can also open the csv file using a text editor, we could see the values are separated by the commas. 

![Open_csv_text](images/11.02.02-Open_csv_text.png "Open the csv file using a text editor")


### Read a CSV file

As before, we could read in the csv file using the *np.loadtxt* function. Let's read in the csv file we just saved to the disk to a variable *my_csv*, and output the first 5 rows. Note here we use the *delimiter* again to specify that the data in the file is separated by commas. 

my_csv = np.loadtxt('./test.csv', delimiter=',')
my_csv[:5, :]

### Beyond Numpy

Numpy is very convenient to deal with csv files, but there are definitely more packages could handle csv files. A very popular one is the *Pandas* package that could easily deal with the tabular data in the Dataframe, you can check it out if you are interested in learning more. 