# Inviscid Burgers

In this notebook, we will explore the first-order upwind scheme for the burgers equation.

To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.

To get started, import the required Python modules by running the cell below.

# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules
import numpy as np
import matplotlib.pyplot as plt

# Import figure style and custom functions
import nbtools as nb

Run the cell containing the function ``burgers``. Read the comments to understand each of the steps.

def burgers(L, n, dt, tf):
    # Build grid
    dx = L/n
    x = np.linspace(0, L - dx, n)
    
    # Initialize solution
    u = np.exp(-40*(x-1/2)**2)
    ut = np.zeros(u.shape)

    # Advance solution in time
    t = 0
    while(t < tf):
        for i in range(n):
            # Enforce periodic boundary condition at x=0
            if i == 0:
                ut[i] = u[i] - 0.5*dt/dx * (u[i]**2-u[n-1]**2)
            else:
                ut[i] = u[i] - 0.5*dt/dx * (u[i]**2-u[i-1]**2)

        u[:] = ut[:]
        t += dt
        
    plt.plot(x, u, 'o-', markersize=2, label=f'$n={n}$')
    plt.legend()

Create a matplotlib figure and add labels to each axis. Plots will appear in this figure.

**Note**: You can always run this cell again to clear the figure.

plt.figure(0)
plt.xlabel('$x$')
plt.ylabel('$u(x,t)$')

Now run the function ``burgers`` providing
   - ``L``: The domain length equal to ``1``,
   - ``n``: The number of grid points equal to ``10``,
   - ``dt``: The time step size equal to ``0.005``,
   - ``tf``: The final time equal to ``0.5``.

# Make sure plot uses figure above
plt.figure(0)

# Assign the provided values to the following variables
L = 
n = 
dt = 
tf = 

burgers(L, n, dt, tf)

Now, call the function ``burgers`` again with 20, 40, 80 and 160 grid points. Keep the rest of the variables the same. What behaviour do you observe? The result must be equal to the figure shown in the Burgers section of the Finite Difference chapter.

# Make sure plot uses figure above
plt.figure(0)

# Call burgers in this cell as many times as required. 
# You may use a for loop. 
# Note that the figure will be updated above.