## Taylor Series

In this notebook, we will explore approximating functions using Taylor series.

To run each of the following cells, use the keyboard shortcut **SHIFT** + **ENTER**, press the button ``Run`` in the toolbar or find the option ``Cell > Run Cells`` from the menu bar. For more shortcuts, see ``Help > Keyboard Shortcuts``.

To get started, import the required Python modules by running the cell below.

# Configuration for visualizing the plots
%matplotlib notebook
%config InlineBackend.figure_format = 'retina'

# Required modules, functions and variables
from math import factorial
import numpy as np
import sympy as sp
from sympy.abc import x
import matplotlib.pyplot as plt

#import nbtools as nb

# Allow printing sympy's math using latex
#sp.init_printing()

Define the function ``f(x)`` to be approximated. Some common functions you may find useful are
   - ``sp.sin(x)``,
   - ``sp.cos(x)``,
   - ``sp.exp(x)``,
   
as an example, we use the sine function. 

f = sp.sin(x)

Define an expansion point, which we call ``a``. 

a = 0

Define the power of the highest-degree expansion ``n``. This is equal to the number of terms + 1. Following the example shown in the Taylor Series Chapter, we define ``n=9``.

n = 9

Run the cell containing the function ``taylor_series``. Read the comments describing each of the steps.

def taylor_series(f, a, n):
    # Initialize plot with exact solution
    fig, ax = plt.subplots()
    colors = nb.get_colors(n+1, end=0.9)
    
    # Define x-axis value range for plotting
    X = np.linspace(-np.pi, np.pi, 100)
    Y = [f.subs(x, xp) for xp in X]
    ax.plot(X, Y, ':k', label='$sin(x)$')
    
    # Initialize fn
    fn = 0
    for i in range(n+1):
        # Get degree i-th derivative and evaluate at point a
        dfn = sp.diff(f, x, i)
        dfn_a = dfn.subs(x, a)
        
        # Add term to fn
        fn += dfn_a*(x-a)**i/factorial(i)
        
        if dfn_a != 0 or i == 0:
            # Plot function only if dfn_a is not zero
            Y = [fn.subs(x, xp) for xp in X]
            ax.plot(X, Y, label=r'$f_{}(x)$'.format(i), color=colors[i], lw=0.9)
    
    # Use multiples of pi/2 to label ticks
    plt.xticks([-np.pi, -np.pi/2, 0, np.pi/2, np.pi], 
               [r'$-\pi$', r'$-\frac{\pi}{2}$', '$0$',
                r'$\frac{\pi}{2}$', r'$\pi$'])
    
    ax.set_ylim(-1.2, 1.2)
    ax.legend()
    return fn

Using the values we have previously defined, we call the ``taylor_series`` function, which will plot the resulting polynomials as the number of terms is increased.

fn = taylor_series(f, a, n)

Print the approximation of $sin(x)$.

fn

Why do only odd-powered terms appear in ``fn`` in our original example? Repeat the procedure above
using a different expansion point, such as $a=\dfrac{\pi}{4}$.

